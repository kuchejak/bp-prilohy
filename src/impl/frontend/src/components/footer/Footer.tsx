import styles from "./Footer.module.css";

/**
 * Spodní lišta zobrazující verzi informačního portálu. Pomocí barvy také signalizuje, v jakém prostředí aplikace běží:
 * bílá = produkce, zelená = dev, béžová = test
 * @constructor
 */
export default function Footer() {
    const getFooterColor = () => {
        switch (process.env.NODE_ENV) {
        case ("development"): return "#A0C814";
        case ("test"): return "#FECD57";
        default: return "var(--color-bcg)";
        }
    };
    return (
        <footer className={styles.footer} style={{ backgroundColor: getFooterColor() }}>
            <p>
                SKP PP, FE
                {" "}
                {process.env.NODE_ENV}
                {" "}
                build v.
                {process.env.REACT_APP_VERSION}
            </p>
        </footer>
    );
}
