import { render, screen } from "@testing-library/react";
import Footer from "./Footer";

describe("Footer", () => {
    test("display basic info from environmental variables", () => {
        render(<Footer />);
        const footer = screen.getByRole("contentinfo");
        expect(footer.textContent).toContain(process.env.NODE_ENV);
        expect(footer.textContent).toContain(process.env.REACT_APP_VERSION);
    });
});
