import EntityDetail, { EntityDetailProps } from "./EntityDetail";
import Questions from "../questions/Questions";
import styles from "./EntityDetailWithQuestions.module.css";

interface Props extends EntityDetailProps {
    questionsUrl?: string;
}

/**
 * Komponeta pro zobrazení detailních informací v info panelu spolu s otázkami
 * @param children Informace o dané entity, předpokládá se použití tagu dl
 * @param title Nadpis
 * @param location Informace o lokaci
 * @param shouldDisplayLocation Určuje, jestli se mají zobrazit informace o lokaci
 * @param editButtons Tlačíka pro úpravu daného záznamu (zobrazená vedle nadpisu)
 * @param questionsUrl Url pro získání otázek a odpovědí
 * @constructor
 */
export default function EntityDetailWithQuestions({
    title, children, shouldDisplayLocation, location, editButtons, questionsUrl,
}: Props) {
    return (
        <div className={styles.flexWrapper}>
            <EntityDetail
                title={title}
                shouldDisplayLocation={shouldDisplayLocation}
                location={location}
                editButtons={editButtons}
            >
                {children}
            </EntityDetail>
            <Questions questionUrl={questionsUrl} />
        </div>
    );
}
