import React from "react";
import LocationDetail from "../location-detail/LocationDetail";
import { Location } from "../../../util/backend/locations/LocationsUtils";
import styles from "./EntityDetail.module.css";

export interface EntityDetailProps {
    children?: React.ReactNode
    editButtons?: React.ReactNode,
    title: string,
    location?: Location,
    shouldDisplayLocation: boolean,
}

/**
 * Komponeta pro zobrazení detailních informací v info panelu
 * @param children Informace o dané entity, předpokládá se použití tagu dl
 * @param title Nadpis
 * @param location Informace o lokaci
 * @param shouldDisplayLocation Určuje, jestli se mají zobrazit informace o lokaci
 * @param editButtons Tlačíka pro úpravu daného záznamu (zobrazená vedle nadpisu)
 */
export default function EntityDetail({
    title, children, shouldDisplayLocation, location, editButtons,
}: EntityDetailProps) {
    return (
        <div className={styles.entityDetail}>
            <div className={styles.headingRow}>
                <h2>{title}</h2>
                <div>
                    {editButtons}
                </div>
            </div>
            {shouldDisplayLocation && location
                && (
                    <>
                        <LocationDetail location={location} />
                        <br />
                    </>
                )}
            {children}
        </div>
    );
}
