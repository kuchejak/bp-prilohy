import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import { ColumnInstance } from "react-table";
import useOnClickOutside from "../../../../hooks/common/useOnClickOutside";
import styles from "./PopUpColumnPicker.module.css";

interface Position {
    top: number,
    left: number;
}

interface Props {
    setShow: (newValue: boolean) => void
    position: Position,
    columns: ColumnInstance[]
}

/**
 * Vyskakovací okno, které slouží pro výběr sloupců tabulky. Komponenta kontroluje pozici a pokud by se mělo menu zobrazit
 * useknuté pravým okrajem, posune menu doleva.
 * @param setShow Funkce, která se spustí po uživatelově výběru sloupce z nabídky
 * @param position Pozice na které se má menu vykreslit
 * @param columns Pole sloupců, ze který si může uživatel vybrat. Typicky "allColumns" z useTable
 */
export default function PopUpColumnPicker({
    setShow, position, columns,
}: Props) {
    const menuRef = React.useRef<HTMLDivElement>(null);
    useOnClickOutside(menuRef, () => { setShow(false); });

    /**
     * Zjišťováný šířky menu nutné pro výpočet pozice
     */
    const [menuWidth, setMenuWidth] = React.useState(0);
    React.useLayoutEffect(() => {
        if (menuRef.current) {
            setMenuWidth(menuRef.current.getBoundingClientRect().width);
        }
    }, [menuRef]);

    /**
     * Funkce kontroluje pozici a šířku elementu + šířku okna a vypočítává, jestli se menu zobrazí viditelné. Pokud
     * zjistí, že menu by se zobrazilo useknuté ("za pravým okrajem"), posune menu doleva.
     */
    const calcLeft = () => {
        const windowWidth = document.documentElement.clientWidth;
        if (windowWidth - position.left < menuWidth) {
            return position.left - menuWidth;
        }
        return position.left;
    };

    return (
        <div
            ref={menuRef}
            className={styles.popUpMenu}
            style={{
                top: position.top,
                left: calcLeft(),
            }}
        >
            {columns.map((column) => (
                <div
                    onClick={() => column.toggleHidden(column.isVisible)}
                    key={column.id}
                    className={`${styles.clm} ${!column.isVisible ? styles.clmHidden : ""}`}
                >
                    <FontAwesomeIcon
                        icon={faCircle}
                        size="xs"
                        className={`${styles.icon} ${!column.isVisible ? styles.iconHidden : ""}`}
                    />
                    {column.Header}
                </div>
            ))}
        </div>
    );
}
