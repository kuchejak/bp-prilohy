import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Table from "./Table";

const header1 = "Column 1";
const header2 = "Column 2";

const cell1 = "cell1";
const cell2 = "cell2";
const cell3 = "cell3";
const cell4 = "cell4";

describe("Table", () => {
    it("should simply display given data", () => {
        const columns = [
            {
                Header: header1,
                accessor: "clm1",
            },
            {
                Header: header2,
                accessor: "clm2",
            },
        ];

        const data = [
            {
                id: 1,
                clm1: cell1,
                clm2: cell2,
            },
            {
                id: 2,
                clm1: cell3,
                clm2: cell4,
            },
        ];

        render(<Table columns={columns} data={data} onRowSelect={() => {}} selectedRowIds={{}} />);
        testAllVisible();
    });

    function testAllVisible() {
        screen.getByRole("table");
        expect(screen.getAllByRole("row").length).toBe(3); // nadpisy a dva řádky
        expect(screen.getAllByRole("cell").length).toBe(4); // 4 viditelne buňky
        expect(screen.getAllByRole("columnheader").length).toBe(2); // 2 sloupce = 2 nadpisy
        expect(screen.getByText(cell1)).toBeInTheDocument();
        expect(screen.queryByText(cell2)).toBeInTheDocument(); // Nemá být zobrazeno
        expect(screen.getByText(cell3)).toBeInTheDocument();
        expect(screen.queryByText(cell4)).toBeInTheDocument(); // Nemá být zobrazeno
    }

    it("should allow column to be hidden by default", () => {
        const columns = [
            {
                Header: header1,
                accessor: "clm1",
            },
            {
                Header: header2,
                accessor: "clm2",
                hidden: true,
            },
        ];

        const data = [
            {
                id: 1,
                clm1: cell1,
                clm2: cell2,
            },
            {
                id: 2,
                clm1: cell3,
                clm2: cell4,
            },
        ];

        render(<Table columns={columns} data={data} onRowSelect={() => {}} selectedRowIds={{}} />);
        testClm2IsHidden();
    });

    function testClm2IsHidden() {
        screen.getByRole("table");
        expect(screen.getAllByRole("row").length).toBe(3); // nadpisy a dva řádky
        expect(screen.getAllByRole("cell").length).toBe(2); // 2 viditelne buňky (2 jsou skryté)
        expect(screen.getAllByRole("columnheader").length).toBe(1); // 1 viditelny sloupec = jeden nadpis
        expect(screen.getByText(cell1)).toBeInTheDocument();
        expect(screen.queryByText(cell2)).toBeNull(); // Nemá být zobrazeno
        expect(screen.getByText(cell3)).toBeInTheDocument();
        expect(screen.queryByText(cell4)).toBeNull(); // Nemá být zobrazeno
    }

    it("should allow column to be hidden by user", () => {
        const columns = [
            {
                Header: header1,
                accessor: "clm1",
            },
            {
                Header: header2,
                accessor: "clm2",
            },
        ];

        const data = [
            {
                id: 1,
                clm1: cell1,
                clm2: cell2,
            },
            {
                id: 2,
                clm1: cell3,
                clm2: cell4,
            },
        ];

        render(<Table columns={columns} data={data} onRowSelect={() => {}} selectedRowIds={{}} />);
        testAllVisible();
        const hideColumnsButton = screen.getByRole("button");
        userEvent.click(hideColumnsButton);
        const headersElements = screen.getAllByText(header2);
        userEvent.click(headersElements[0]); // Element ve výběru sloupců
        testClm2IsHidden();
    });

    it("should allow hidden column to be displayed by user", () => {
        const columns = [
            {
                Header: header1,
                accessor: "clm1",
            },
            {
                Header: header2,
                accessor: "clm2",
                hidden: true,
            },
        ];

        const data = [
            {
                id: 1,
                clm1: cell1,
                clm2: cell2,
            },
            {
                id: 2,
                clm1: cell3,
                clm2: cell4,
            },
        ];

        render(<Table columns={columns} data={data} onRowSelect={() => {}} selectedRowIds={{}} />);
        testClm2IsHidden();
        const hideColumnsButton = screen.getByRole("button");
        userEvent.click(hideColumnsButton);
        const headersElements = screen.getAllByText(header2);
        userEvent.click(headersElements[0]); // Element ve výběru sloupců
        testAllVisible();
    });

    it("should call passed in function when row is selected", () => {
        const columns = [
            {
                Header: header1,
                accessor: "clm1",
            },
            {
                Header: header2,
                accessor: "clm2",
            },
        ];

        const data = [
            {
                id: 1,
                clm1: cell1,
                clm2: cell2,
            },
            {
                id: 2,
                clm1: cell3,
                clm2: cell4,
            },
        ];

        const onRowSelect = jest.fn();
        render(<Table columns={columns} data={data} onRowSelect={onRowSelect} selectedRowIds={{}} />);
        userEvent.click(screen.getByText(cell1));
        expect(onRowSelect).toBeCalled();
    });
});
