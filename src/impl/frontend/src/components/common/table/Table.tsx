/* eslint react/prop-types: "off" */
import {
    Cell,
    Column,
    Row,
    TableCellProps, TableHeaderGroupProps,
    useBlockLayout,
    useResizeColumns,
    useRowSelect,
    useTable,
} from "react-table";
import React from "react";
import styles from "./Table.module.css";
import useOnClickOutside from "../../../hooks/common/useOnClickOutside";
import PopUpColumnPicker from "./pop-up-column-picker/PopUpColumnPicker";
import ColorVariant from "../../../util/display/ColorVariant";
import { Links } from "../../../util/backend/common/BackendUtils";

export type HideableColumn = Column & {
    hidden?: boolean
};

export type Data = {} & {
    id: number
    color?: ColorVariant
    _links?: Links
};

interface Props {
    columns: readonly HideableColumn [],
    data: readonly Data[],
    onRowSelect: (row: Data) => void,
    selectedRowIds: Record<string, boolean>
}

/**
 * Komponenta reprezentující základní tabulku. Umožňuje měnit šířku sloupců a výběr řádků.
 * @param columns - Popis jednotlivých sloupců
 * @param data - Data do jednotlivých řádků (musí odpovídat definici z parametru columns)
 * @param onRowSelect - Funkce která se má spustit po té, co uživatel vybere řádek (musí se propagovat i do ostatních
 * tabulek na stránce
 * @param selectedRowIds - Slouží ke kontrole vybraných řádků s pomocí rodičovské komponenty.
 */
export default function Table({
    columns, data, onRowSelect, selectedRowIds,
}: Props) {
    const buttonRef = React.useRef<HTMLButtonElement>(null);
    const menuRef = React.useRef<HTMLDivElement>(null);
    const [showMenu, setShowMenu] = React.useState(false);
    useOnClickOutside(menuRef, () => { /* Slouží pro zavření výběru sloupců kliknutím mimo okno */
        setShowMenu(false);
    });

    const defaultColumn = React.useMemo(
        () => ({
            minWidth: 15,
            width: 150,
            maxWidth: 500,
        }),
        [],
    );

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        allColumns,
    } = useTable(/* Základní inicializace tabulky pomocí knihovny React Table */
        {
            columns,
            data,
            defaultColumn,
            initialState: { /* Stará se o skrytí sloupců, které mají být defaultně skryty */
                hiddenColumns:
                    columns
                        .filter((column) => column.hidden)
                        .map((column) => column.accessor) as string[],

            },
            useControlledState: (state) => /* Řízení vybraného řádku s pomocí propu selectedRowId */
                React.useMemo(
                    () => ({
                        ...state,
                        selectedRowIds,
                    }),
                    [state, selectedRowIds],
                ),

            getRowId: (row) => /* Předefinování defaultního generování id sloupců */
                (row as Data).id.toString(),

        },
        useResizeColumns,
        useBlockLayout,
        useRowSelect,
    );

    const getPosition = (buttonPos: DOMRect) => ({
        top: buttonPos.bottom,
        left: buttonPos.left,
    });

    /**
     * Funkce se startá o obarvení buněk, které jsou na řádku jenž má být obarven
     * @param cell Buňka, která má být zkontrolována/obarvena
     * @param props Props, které vrací getRowProps od React Table
     * @param row Řádek, na kterém se buňka nachází
     */
    const colorCellProps = (cell: Cell, props: Partial<TableCellProps>, row: Row) => {
        const result = props;
        const rowData = row.original as Data;
        if (rowData.color) {
            const color = row.isSelected ? rowData.color.colorSelected : rowData.color.colorNormal;
            result.style = { ...props.style, backgroundColor: color };
        }
        return result;
    };

    /**
     * Nastaví width = 100% pro style zadaných props
     */
    const setWidth100 = (props: Partial<TableHeaderGroupProps>) =>
        ({ ...props, style: { ...props.style, width: "100%" } });

    return (
        <div className={styles.tableWrapper}>
            <div className={styles.overflowBlock}>
                { /* Nabídka pro výběr sloupců */
                    buttonRef.current && showMenu /* div níže se zobrazí pouze pokud jsou obě podmínky splněny */
                    && (
                        <PopUpColumnPicker
                            setShow={setShowMenu}
                            position={getPosition(buttonRef.current.getBoundingClientRect())}
                            columns={allColumns}
                        />
                    )
                }
                <button
                    className={styles.columnSelectionButton}
                    ref={buttonRef}
                    onClick={() => setShowMenu(!showMenu)}
                    type="button"
                >
                    {" "}
                    &rsaquo;
                </button>
                <table {...getTableProps()} className={styles.table}>
                    <thead>
                        {headerGroups.map((headerGroup) => (
                            <tr {...headerGroup.getHeaderGroupProps(setWidth100)}>
                                {headerGroup.headers.map((column) => (
                                    <th {...column.getHeaderProps()}>
                                        {column.render("Header")}
                                        <div
                                            {...column.getResizerProps()}
                                            className={`${styles.resizer} ${
                                                column.isResizing ? styles.isResizing : ""
                                            }`}
                                        />
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>

                    <tbody {...getTableBodyProps()}>
                        {rows.map((row) => {
                            prepareRow(row);
                            return (
                                <tr
                                    onClick={() => {
                                        row.toggleRowSelected(!row.isSelected);
                                        onRowSelect(row.original as Data);
                                    }}
                                    {...row.getRowProps(setWidth100)}
                                >
                                    {row.cells.map((cell) => (
                                        <td className={row.isSelected ? styles.selected : ""} {...cell.getCellProps((props) => colorCellProps(cell, props, row))}>
                                            {cell.render("Cell")}
                                        </td>
                                    ))}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
