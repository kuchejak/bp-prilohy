import { render, screen } from "@testing-library/react";
import LocationDetail from "./LocationDetail";
import { Location } from "../../../util/backend/locations/LocationsUtils";

const location: Location = {
    system: "abc",
    subsystem: "dcb",
    adminWebApplication: "gba",
    userWebApplication: "xyz",
    channel: "zys",
    id: 1,
};

describe("LocationDetail", () => {
    it("should display all passed in information", () => {
        render(<LocationDetail location={location} />);
        screen.getByText("Systém");
        screen.getByText("Subsystém");
        screen.getByText("Uživatelská aplikácia");
        screen.getByText("Administrátorská aplikácia");
        screen.getByText("Kanál");
        screen.getByText(location.system!);
        screen.getByText(location.subsystem!);
        screen.getByText(location.adminWebApplication!);
        screen.getByText(location.userWebApplication!);
        screen.getByText(location.channel!);
    });
});
