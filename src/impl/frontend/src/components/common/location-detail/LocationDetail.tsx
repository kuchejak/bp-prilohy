import { Location } from "../../../util/backend/locations/LocationsUtils";
import { createDtDd } from "../../main/info-panel/InfoPanelUtils";

interface Props {
   location: Location
}
/**
 * Komponenta pro zobrazení detailního popisu lokace, která je získána z backendu pomocí dodaného linku
 * @param link Link odkazující na danou lokaci
 */
export default function LocationDetail({ location }: Props) {
    return (
        <dl>
            {createDtDd("Systém", location.system)}
            {createDtDd("Subsystém", location.subsystem)}
            {createDtDd("Uživatelská aplikácia", location.userWebApplication)}
            {createDtDd("Administrátorská aplikácia", location.adminWebApplication)}
            {createDtDd("Kanál", location.channel)}
        </dl>
    );
}
