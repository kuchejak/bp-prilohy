import React from "react";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./Button.module.css";

export enum Position {
    Left,
    Right,
}

interface Props {
    children?: React.ReactNode
    onClick?: () => void
    icon?: IconProp
    iconPosition?: Position
    className?: string
}

/**
 * Komponenta reprezentující základní tlačítko, s možnosti přidat ikonu
 * @param children "Vnitřek" tlačítka - většinou popisek
 * @param onClick Funkce, která se provede po kliknutí na tlačítko
 * @param icon Ikona, která se má na tlačítku zobrazit
 * @param iconPosition Pozice ikony na tlačítku (vlevo/vpravo)
 * @param className Třída, která se má aplikovat na tlačítko
 */
export default function Button({
    children, onClick, icon, iconPosition = Position.Left, className,
}: Props) {
    const classNameToUse = className || "";
    if (!icon) {
        return (
            <button onClick={onClick} className={`${classNameToUse} ${styles.button}`} type="button">
                {children}
            </button>
        );
    }

    if (iconPosition === Position.Right) {
        return (
            <button onClick={onClick} className={`${classNameToUse} ${styles.button}`} type="button">
                {children}
                <div className={styles.iconHolderRight}><FontAwesomeIcon icon={icon} /></div>
            </button>
        );
    }

    return (
        <button onClick={onClick} className={`${classNameToUse} ${styles.button}`} type="button">
            <div className={styles.iconHolderLeft}><FontAwesomeIcon icon={icon} /></div>
            {children}
        </button>
    );
}
