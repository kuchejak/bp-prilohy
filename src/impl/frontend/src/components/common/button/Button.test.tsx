import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import Button from "./Button";

describe("Button", () => {
    it("should display text", () => {
        const buttonText = "Text Text";
        render(<Button>{buttonText}</Button>);
        screen.getByRole("button");
        screen.getByText(buttonText);
    });

    it("should support custom class", () => {
        const className = "TestClassName";
        render(<Button className={className} />);
        const button = screen.getByRole("button");
        expect(button.className).toContain(className);
    });

    it("should use passed in function on click", () => {
        const onClick = jest.fn();
        render(<Button onClick={onClick} />);
        const button = screen.getByRole("button");
        userEvent.click(button);
        expect(onClick).toBeCalled();
    });

    it("should display icon", () => {
        const buttonText = "Text Text";
        render(<Button icon={faBook}>{buttonText}</Button>);
        const button = screen.getByRole("button");
        expect(button.children.item(0)).toBeInTheDocument();
        screen.getByText(buttonText);
    });
});
