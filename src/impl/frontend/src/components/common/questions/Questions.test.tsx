import { render, screen } from "@testing-library/react";
import Questions from "./Questions";

const hookReturn = {
    _embedded: {
        questionWithAnswerList: [
            {
                id: 1,
                question: "abc?",
                answer: "abc",
            },
            {
                id: 2,
                question: "ttt?",
                answer: "sss",
            },
        ],
    },
};

jest.mock("../../../hooks/common/backend/useBackendGet", () => () => hookReturn);

describe("Questions", () => {
    it("should display buttons for adding new questions", () => {
        const link = "https://test.com/test";
        render(<Questions questionUrl={link} />);
        const buttons = screen.getAllByRole("button").map((btn) => btn.textContent);
        expect(buttons).toContain("Pridať otázku s odpoveďou");
        expect(buttons).toContain("Položiť otázku");
    });

    it("should display questions and answers", () => {
        const link = "https://test.com/test";
        render(<Questions questionUrl={link} />);
        const qna = hookReturn._embedded.questionWithAnswerList;
        screen.getByText(qna[0].question);
        screen.getByText(qna[0].answer);
        screen.getByText(qna[1].question);
        screen.getByText(qna[1].answer);
    });
});
