import {
    faComment, faComments,
} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import styles from "./Questions.module.css";
import entityDetailStyles from "../entity-detail/EntityDetail.module.css";
import Button from "../button/Button";
import useBackendGet from "../../../hooks/common/backend/useBackendGet";
import {
    QuestionsWithAnswersEmbedded,
    QuestionWithAnswer,
} from "../../../util/backend/questions/QuestionsUtils";

interface Props {
    questionUrl?: string;
}

/**
 * Komponenta zobrazující otázky a odpovědi
 * @param questionUrl URL na kterém je možné získat otázky a odpovědi k zobrazení
 */
export default function Questions({ questionUrl }: Props) {
    let qna: QuestionWithAnswer[] | undefined;

    if (questionUrl) {
        qna = useBackendGet<QuestionsWithAnswersEmbedded>(questionUrl)
            ?._embedded?.questionWithAnswerList;
    }

    return (
        <div className={styles.questionsWrapper}>
            <div className={entityDetailStyles.headingRow}>
                <h2>Otázky</h2>
                <div>
                    <Button icon={faComment}>Položiť otázku</Button>
                    <Button icon={faComments}>Pridať otázku s odpoveďou</Button>
                </div>
            </div>
            {
                questionUrl && qna
                && (
                    <dl>
                        {qna.map((q) => (
                            <React.Fragment key={q.id}>
                                <dt>{q.question}</dt>
                                <dd>{q.answer}</dd>
                            </React.Fragment>
                        ))}
                    </dl>
                )
            }
        </div>
    );
}
