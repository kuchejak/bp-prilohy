import { render, screen } from "@testing-library/react";
import RowInformation from "./RowInformation";
import { DowntimesOngoingColor, WHITE } from "../../../../util/display/ColorVariant";

describe("RowInformation", () => {
    it("should display passed in rows", () => {
        const description1 = "Description 1";
        const description2 = "Description 2";

        const rows = [
            {
                description: description1,
                color: DowntimesOngoingColor,
            },
            {
                description: description2,
                color: {
                    colorNormal: WHITE,
                    colorSelected: WHITE,
                },
            },
        ];
        render(<RowInformation rows={rows} />);
        screen.getByText(description1);
        screen.getByText(description2);
    });
});
