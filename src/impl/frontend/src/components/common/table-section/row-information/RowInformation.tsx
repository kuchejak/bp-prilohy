import { faSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import ColorVariant, { WHITE } from "../../../../util/display/ColorVariant";
import styles from "./RowInformation.module.css";

interface Props {
    rows: {
        description: string,
        color: ColorVariant
    }[]
}

/**
 * Komponenta reprezentující jednoduchý tooltip, který se zobrazí po najetí myší. Umožňuje zobrazit řádky a přiřadit k nim
 * barvu.
 * @param rows Řádky tooltipu
 */
export default function RowInformation({ rows }: Props) {
    /**
     * Funkce se stará o to, aby byl čtvereček reprezentující barvu správně vybarven  - pokud je barva bílá, je mu navíc
     * přidán rámeček rámeček, aby byl vidět
     * @param color
     */
    const getIconStyles = (color: string) : React.CSSProperties => {
        if (color !== WHITE) {
            return { color };
        }
        return {
            color, border: "thin black solid", borderRadius: "2px", width: "10px", height: "10px",
        };
    };

    return (
        <>
            {
                rows.map((row, index) => (
                    // Zde použití indexu nevadí, protože se pole nikdy nemění
                    // eslint-disable-next-line react/no-array-index-key
                    <div key={index} className={styles.row} style={{ backgroundColor: row.color.colorNormal }}>
                        <FontAwesomeIcon icon={faSquare} style={getIconStyles(row.color.colorSelected)} />
                        {" "}
                        <span className={styles.rowDescription}>{row.description}</span>
                    </div>
                ))
            }
        </>
    );
}
