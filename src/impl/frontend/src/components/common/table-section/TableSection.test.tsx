import { render, screen } from "@testing-library/react";
import TableSection from "./TableSection";

const header1 = "Column 1";
const header2 = "Column 2";

const cell1 = "cell1";
const cell2 = "cell2";
const cell3 = "cell3";
const cell4 = "cell4";

const heading = "Heading 1";

const columns = [
    {
        Header: header1,
        accessor: "clm1",
    },
    {
        Header: header2,
        accessor: "clm2",
    },
];

const data = [
    {
        id: 1,
        clm1: cell1,
        clm2: cell2,
    },
    {
        id: 2,
        clm1: cell3,
        clm2: cell4,
    },
];

describe("Table section", () => {
    it("should display passed in heading and table with passed in data", () => {
        render(<TableSection heading={heading} columns={columns} data={data} onRowSelect={() => {}} selectedRowIds={{}} />);
        screen.getByText(heading);
        screen.getByRole("table");
    });
});
