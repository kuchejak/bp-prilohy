import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import styles from "./TableSection.module.css";
import Table, { HideableColumn, Data } from "../table/Table";

interface Props {
    heading: string
    tooltip?: JSX.Element
    columns: readonly HideableColumn [],
    data: readonly Data[],
    onRowSelect: (row: Data) => void,
    selectedRowIds: Record<string, boolean>,
}

/**
 * Sekce s nadpisem a odpovídající tabulkou
 * @param heading Nadpis sekce
 * @param tooltip Tooltip u nadpisu
 * @param columns Sloupce tabulky
 * @param data Data tabulky
 * @param onRowSelect Funkce, která se provede po vybrání řádku z tabulky
 * @param selectedRowIds Id vybraných řádků v tabulce
 */
export default function TableSection({
    heading, tooltip, columns, data, onRowSelect, selectedRowIds,
}: Props) {
    return (
        <section className={styles.section}>
            <h3>{heading}</h3>
            {tooltip
            && (
                <div className={styles.tooltipHolder}>
                    <FontAwesomeIcon icon={faQuestion} className={styles.icon} size="xs" />
                    <div className={styles.tooltip}>{tooltip}</div>
                </div>
            )}

            <Table
                columns={columns}
                data={data}
                onRowSelect={onRowSelect}
                selectedRowIds={selectedRowIds}
            />
        </section>
    );
}
