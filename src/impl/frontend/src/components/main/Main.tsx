import Split from "react-split";
import React, { useState } from "react";
import MenuPanel from "./menu-panel/MenuPanel";
import MainPanel from "./main-panel/MainPanel";
import InfoPanel from "./info-panel/InfoPanel";
import styles from "./Main.module.css";
import { RowContext, SelectedRowContext } from "../../contexts/SelectedRowContext";
import { MenuItem, SelectedMenuItemContext } from "../../contexts/SelectedMenuItemContext";

/**
 * Hlavní část aplikace, obsahuje tři sloupce - menu, tabulky a podrobné informace
 */
export default function Main() {
    const [selectedRow, setSelectedRow] = useState<RowContext | undefined>();
    const [selectedMenuItem, setSelectedMenuItem] = useState<MenuItem | undefined>();

    /*
     * Funkce se stará o to, aby šlo řádek jak vybrat v případě že vybraný není, tak "odvybrat" v případě že už vybraný je
     */
    const toggleSelected = React.useMemo(() => (rowContext: RowContext | undefined) => {
        if (!rowContext) {
            setSelectedRow(undefined);
        } else if (selectedRow?.section === rowContext.section && selectedRow?.id === rowContext.id) { // Řádek je již vybraný => Odvybrání
            setSelectedRow(undefined);
        } else { // Řádek není vybraný => Vybrání
            setSelectedRow(rowContext);
        }
    }, [selectedRow, setSelectedRow]);

    const selectedMenuItemContextValue = React.useMemo(
        () => ({ selectedMenuItem, setSelectedMenuItem }),
        [selectedMenuItem, setSelectedMenuItem],
    );

    const selectedRowContextValue = React.useMemo(
        () => ({ selectedRow, setSelectedRow: toggleSelected }),
        [selectedRow, toggleSelected],
    );

    return (
        <main className={styles.main}>
            <SelectedMenuItemContext.Provider value={selectedMenuItemContextValue}>
                <SelectedRowContext.Provider value={selectedRowContextValue}>
                    <MenuPanel />
                    <Split
                        className={styles.split}
                        cursor="col-resize"
                        sizes={[50, 50]} // Rozdělení šířky v procentech
                        minSize={0}
                        gutterSize={6}
                    >
                        <div className={styles.splitPanel}>
                            <MainPanel />
                        </div>
                        <div className={styles.splitPanel}>
                            <InfoPanel />
                        </div>
                    </Split>
                </SelectedRowContext.Provider>
            </SelectedMenuItemContext.Provider>
        </main>
    );
}
