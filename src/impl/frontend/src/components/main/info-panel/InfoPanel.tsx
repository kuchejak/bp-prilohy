import React from "react";
import styles from "./InfoPanel.module.css";
import { MainPanelSection, SelectedRowContext } from "../../../contexts/SelectedRowContext";
import DocumentDetail from "./document-detail/DocumentDetail";
import DowntimeDetail from "./downtime-detail/DowntimeDetail";
import { SelectedMenuItemContext } from "../../../contexts/SelectedMenuItemContext";

/**
 * Vertikální panel s podrobnými informacemi (většinou o vybraném řádku)
 */
export default function InfoPanel() {
    const { selectedRow } = React.useContext(SelectedRowContext);
    const { selectedMenuItem } = React.useContext(SelectedMenuItemContext);

    let content = (
        <>
            <h2>Neplatný riadok</h2>
            <p>Kontaktuje prosím správca systému</p>
        </>
    );

    // Odkaz na informace pro zobrazení v info panelu
    const selfLink = selectedRow?._links?.self;

    if (!selectedRow) {
        content = <h2>Riadok nie je vybratý</h2>; // TODO zobrazení informací o zvolené části IS SKP
    } else if (selfLink) {
        const shouldDisplayLocation = selectedMenuItem?.specialType === "Home";
        switch (selectedRow.section) {
        case MainPanelSection.Document: {
            content = <DocumentDetail entityUrl={selfLink.href} shouldDisplayLocation={shouldDisplayLocation} />;
            break;
        }
        case MainPanelSection.Downtime: {
            content = <DowntimeDetail entityUrl={selfLink.href} shouldDisplayLocation={shouldDisplayLocation} />;
            break;
        }
        default: {
            break;
        }
        }
    }

    return (
        <div className={styles.infoPanel}>
            {content}
        </div>
    );
}
