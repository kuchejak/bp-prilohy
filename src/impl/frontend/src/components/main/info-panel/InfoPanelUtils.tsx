import { UrlProps } from "../../../util/backend/common/BackendUtils";

export interface DetailProps extends UrlProps {
    shouldDisplayLocation: boolean;
}

/**
 * Funkce, pro vytváření dvojic <dt> a <dd> tagů (pokud je k dispozici text pro <dd>)
 * @param dt Text pro <dt> tag
 * @param dd Text pro <dd> tag
 */
export function createDtDd(dt: string, dd: string | undefined | null) {
    if (dd) {
        return (
            <>
                <dt>{dt}</dt>
                <dd>{dd}</dd>
            </>
        );
    }
    return null;
}
