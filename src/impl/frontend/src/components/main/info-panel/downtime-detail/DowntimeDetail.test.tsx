import { render, screen } from "@testing-library/react";
import DowntimeDetail from "./DowntimeDetail";

const mockDown = {
    id: 1,
    from: "1. 3. 2022",
    to: "2. 3. 2022",
    reason: "Test reason",
    location: { id: 1, system: "DKP" },
    downtimeNumber: "2022/13",
    additionalInfo: "Test additional info",
    accessPermission: "TEST_Perm",
};

jest.mock("../../../../hooks/downtimes/useGetDowntime", () => () => mockDown);

describe("DowntimeDetail", () => {
    it("should display information about downtime", () => {
        const link = "https://test.com/test";
        render(<DowntimeDetail entityUrl={link} shouldDisplayLocation />);

        // Popisky
        const allTerms = screen.getAllByRole("term").map((el) => el.textContent);
        expect(allTerms).toStrictEqual(["Systém", "Dôvod", "Začiatok", "Koniec", "Doplňujúce informácie", "Práva"]);

        // Hodnoty
        screen.getByText(`Odstávka ${mockDown.downtimeNumber}`);
        const allDefs = screen.getAllByRole("definition").map((el) => el.textContent);
        expect(allDefs).toStrictEqual([mockDown.location.system, mockDown.reason, mockDown.from, mockDown.to, mockDown.additionalInfo, mockDown.accessPermission]);
    });

    it("should display buttons for downtime actions", () => {
        const link = "https://test.com/test";
        render(<DowntimeDetail entityUrl={link} shouldDisplayLocation />);
        const buttons = screen.getAllByRole("button").map((button) => button.textContent);
        expect(buttons).toContain("Upraviť odstávku");
        expect(buttons).toContain("Zmazať odstávku");
    });
});
