import {
    faEdit, faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import useGetDowntime from "../../../../hooks/downtimes/useGetDowntime";
import { createDtDd, DetailProps } from "../InfoPanelUtils";
import Button from "../../../common/button/Button";
import EntityDetailWithQuestions from "../../../common/entity-detail/EntityDetailWithQuestions";

/**
 * Komponeta pro zobrazení detailních informací o odstávce, která je získána z backendu pomocí linku na vstupu
 * @param link Link na danou odstávku
 * @param shouldDisplayLocation Určuje, jestli mají být zobrazeny informace o lokaci
 */
export default function DowntimeDetail({
    entityUrl,
    shouldDisplayLocation,
}: DetailProps) {
    const downtime = useGetDowntime(entityUrl.toString());
    if (downtime) {
        return (
            <EntityDetailWithQuestions
                questionsUrl={downtime._links?.questions.href}
                title={`Odstávka ${downtime?.downtimeNumber}`}
                shouldDisplayLocation={shouldDisplayLocation}
                location={downtime?.location}
                editButtons={(
                    <div>
                        <Button icon={faEdit}>Upraviť odstávku</Button>
                        <Button icon={faTrashAlt}>Zmazať odstávku</Button>
                    </div>
                )}
            >
                <dl>
                    {createDtDd("Dôvod", downtime.reason)}
                    {createDtDd("Začiatok", downtime.from)}
                    {createDtDd("Koniec", downtime.to)}
                    {createDtDd("Doplňujúce informácie", downtime.additionalInfo)}
                    {createDtDd("Práva", downtime.accessPermission)}
                </dl>
            </EntityDetailWithQuestions>
        );
    }
    return null;
}
