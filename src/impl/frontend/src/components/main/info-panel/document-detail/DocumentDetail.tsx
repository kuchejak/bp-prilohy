import {
    faDownload, faPlus, faEdit, faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import useGetDocument from "../../../../hooks/documents/useGetDocument";
import Button from "../../../common/button/Button";
import { createDtDd, DetailProps } from "../InfoPanelUtils";
import EntityDetailWithQuestions from "../../../common/entity-detail/EntityDetailWithQuestions";

/**
 * Komponeta pro zobrazení detailních informací o dokumentu, které jsou zíkány z backendu pomocí linku na vstupu.
 * @param link Link na danou informace o daném dokumentu.
 * @param shouldDisplayLocation Určuje, jestli mají být zobrazeny informace o lokaci
 */
export default function DocumentDetail({ entityUrl, shouldDisplayLocation }: DetailProps) {
    const document = useGetDocument(entityUrl);
    if (document) {
        return (
            <EntityDetailWithQuestions
                questionsUrl={document._links?.questions.href}
                title={`Dokument ${document?.documentNumber}`}
                shouldDisplayLocation={shouldDisplayLocation}
                location={document?.location}
                editButtons={(
                    <div>
                        <Button icon={faPlus}>Nová verzia</Button>
                        <Button icon={faEdit}>Upraviť dokument</Button>
                        <Button icon={faTrashAlt}>Zmazať dokument</Button>
                    </div>
                )}
            >
                <dl>
                    {createDtDd("Názov", document.title)}
                    {createDtDd("Typ dokumentu", document.documentType?.description)}
                    {createDtDd("Verzia", document.version)}
                    {createDtDd("Dôvod vloženia", document.reasonForInsertion)}
                    {createDtDd("Dátum zmeny", document.dateOfChange)}
                    {createDtDd("Práva", document.accessPermission)}
                    <dt>Obsah</dt>
                    <dd><Button icon={faDownload}>Stiahnuť</Button></dd>
                </dl>
            </EntityDetailWithQuestions>
        );
    }
    return null;
}
