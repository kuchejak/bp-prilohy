import { render, screen } from "@testing-library/react";
import DocumentDetail from "./DocumentDetail";

const mockDoc = {
    id: 1,
    title: "TestDocument",
    accessPermission: "ALL",
    dateOfChange: "10. 1. 2022",
    documentNumber: 50020,
    documentType: {
        description: "Testovací dokument",
        extension: "PDF",
    },
    location: { id: 12, system: "DKP" },
    reasonForInsertion: "Nová verze",
    version: "1.0.0",
};

jest.mock("../../../../hooks/documents/useGetDocument", () => () => mockDoc);

describe("DocumentDetail", () => {
    it("should display information about document", () => {
        const link = "https://test.com/test";
        render(<DocumentDetail entityUrl={link} shouldDisplayLocation />);

        // Popisky
        const allTerms = screen.getAllByRole("term").map((el) => el.textContent);
        expect(allTerms).toStrictEqual(["Systém", "Názov", "Typ dokumentu", "Verzia", "Dôvod vloženia", "Dátum zmeny", "Práva", "Obsah"]);

        // Hodnoty
        screen.getByText(`Dokument ${mockDoc.documentNumber}`);
        const allDefs = screen.getAllByRole("definition").map((el) => el.textContent);
        expect(allDefs).toStrictEqual([mockDoc.location.system, mockDoc.title, mockDoc.documentType.description,
            mockDoc.version, mockDoc.reasonForInsertion, mockDoc.dateOfChange, mockDoc.accessPermission, "Stiahnuť"]);

        // Tlačítko
        const buttons = screen.getAllByRole("button").map((button) => button.textContent);
        expect(buttons).toContain("Stiahnuť");
    });

    it("should display buttons for document actions", () => {
        const link = "https://test.com/test";
        render(<DocumentDetail entityUrl={link} shouldDisplayLocation />);
        const buttons = screen.getAllByRole("button").map((button) => button.textContent);
        expect(buttons).toContain("Nová verzia");
        expect(buttons).toContain("Upraviť dokument");
        expect(buttons).toContain("Zmazať dokument");
    });
});
