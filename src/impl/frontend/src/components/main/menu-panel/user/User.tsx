/**
 * Komponenta pro zobrazení příhlášeného uživatele.
 */
export default function User() {
    const user = "kuchejda"; // Prozatím zobrazuje vždy stejnou hodnotu
    return (
        <span>
            Login:
            {" "}
            {user}
        </span>
    );
}
