import React from "react";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./MenuPanel.module.css";
import Menu from "./menu/Menu";
import DocumentSearch from "./document-search/DocumentSearch";

/**
 * Vertikální panel s uživatelem, menu a vyhledáváním dokumentů
 */
export default function MenuPanel() {
    const DEFAULT_WIDTH = "315px";
    const HIDDEN_WIDTH = "15px";
    const [isHidden, setIsHidden] = React.useState(false);
    const [panelWidth, setPanelWidth] = React.useState(DEFAULT_WIDTH);

    const toggleHidden = () => {
        if (isHidden) {
            setPanelWidth(DEFAULT_WIDTH);
            setIsHidden(false);
        } else {
            setPanelWidth(HIDDEN_WIDTH);
            setIsHidden(true);
        }
    };

    return (
        <div className={styles.menuPanel} style={{ width: panelWidth, minWidth: panelWidth }}>
            {
                !isHidden
                && (
                    <div className={styles.contentWrapper}>
                        <DocumentSearch />
                        <Menu />
                    </div>
                )
            }
            <button className={styles.hideBar} onClick={toggleHidden} type="button">
                <FontAwesomeIcon icon={isHidden ? faAngleRight : faAngleLeft} className={styles.showIcon} />
            </button>
        </div>
    );
}
