import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { faCaretDown, faCaretRight } from "@fortawesome/free-solid-svg-icons";
import menuStyles from "../Menu.module.css";

interface Props {
    title: string
    hiddenByDefault: boolean
    onClick: () => void
    isSelected: boolean
    children?: React.ReactNode
}

/**
 * Prvek tabulky s potomky, kteří se dají zobrazit/skrýt klinutím na tlačítko
 * @param title Název položky
 * @param hiddenByDefault Určuje, jestli jsou potomci defaultně skrytí
 * @param onClick Funkce, která se provede po klinutí na položku
 * @param isSelected Určuje, jestli je položka vybrána (používá se pro nastavení CSS tříd)
 * @param children Potomci
 */
export default function HideableListItem({
    title, hiddenByDefault, onClick, isSelected, children,
}: Props) {
    const [hidden, setHidden] = useState(hiddenByDefault);

    function getIcon() {
        if (hidden) return faCaretRight;
        return faCaretDown;
    }

    return (
        <>
            <li>
                <div className={menuStyles.iconHolder} onClick={() => setHidden(!hidden)}>
                    <FontAwesomeIcon icon={getIcon()} size="1x" />
                </div>
                <span onClick={onClick} className={`${isSelected ? menuStyles.selected : ""} ${menuStyles.clickable} ${menuStyles.itemTitle}`}>
                    {title}
                </span>
            </li>
            {
                !hidden && children
            }
        </>
    );
}
