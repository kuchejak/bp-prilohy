import { render, screen } from "@testing-library/react";
import Menu from "./Menu";
import useGetLocationsForMenu from "../../../../hooks/locations/useGetLocationsForMenu";

const menuItems = [
    {
        id: 1,
        name: "Domov",
        specialType: "Home",
        children: null,
    },
    {
        id: 2,
        name: "IS SKP",
        specialType: "SystemRoot",
        children: [
            {
                id: 3,
                name: "DKP",
                specialType: null,
                children: null,
            },
        ],
    },
];

jest.mock("../../../../hooks/locations/useGetLocationsForMenu", () => () => menuItems);

describe("Menu", () => {
    useGetLocationsForMenu();
    it("should display fetched locations", () => {
        render(<Menu />);
        menuItems.forEach((menuItem) => {
            screen.getByText(menuItem.name);
            menuItem.children?.forEach((child) => {
                screen.getByText(child.name);
            });
        });
    });
});
