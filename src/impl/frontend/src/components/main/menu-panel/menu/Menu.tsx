import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import HideableListItem from "./hideable-list-item/HideableListItem";
import { SelectedMenuItemContext, MenuItem } from "../../../../contexts/SelectedMenuItemContext";
import styles from "./Menu.module.css";
import useGetLocationsForMenu from "../../../../hooks/locations/useGetLocationsForMenu";
import { SelectedRowContext } from "../../../../contexts/SelectedRowContext";

interface FullMenuItem extends MenuItem {
    icon?: IconProp
    children?: FullMenuItem[]
}

/**
 * Komponenta pro zobrazující menu aplikace
 */
export default function Menu() {
    const { selectedMenuItem, setSelectedMenuItem } = React.useContext(SelectedMenuItemContext);
    const { setSelectedRow } = React.useContext(SelectedRowContext);
    const [isFirst, setIsFirst] = React.useState(true);

    const locations = useGetLocationsForMenu();

    // Automaticky vybere Domov po prvním načtení stránky
    const home = locations.find((location) => location.specialType === "Home");
    if (isFirst && home) {
        setIsFirst(false);
        setSelectedMenuItem(home);
    }

    /**
     * Funkce, která pomocí rekurze vykresluje menu
     * @param item Menu položka, která se má vykreslit
     * @param index Index položky v rodičovském listu (používá se pro key)
     * @param depth Hloubka zanoření (obzvlášť nultá úroveň má speciální pravidla)
     */
    function renderItem(item: FullMenuItem, index: React.Key, depth: number): JSX.Element {
        if (depth === 0 || !item.children) { // Položky na nulté úrovni mají speciální pravidla (Pod-obsah nejde skrýt)
            return (
                <li key={index}>
                    <span
                        onClick={() => {
                            setSelectedMenuItem({
                                id: item.id, name: item.name, icon: item.icon, specialType: item.specialType, _links: item._links,
                            });
                            setSelectedRow(undefined); // Odvybrání vybraného řádku při změně výberu v menu
                        }}
                        className={`${item.icon && item.id === selectedMenuItem?.id ? styles.selected : ""} ${styles.clickable}`}
                    >
                        {/* Div je je používán aby se v případě neexistující ikony zachovalo prázné místo před textem */}
                        <div className={styles.iconHolder}>
                            {item.icon && <FontAwesomeIcon icon={item.icon} size="sm" />}
                        </div>
                        <span className={`${item.id === selectedMenuItem?.id ? styles.selected : ""} ${styles.itemTitle}`}>
                            {item.name}
                        </span>
                    </span>
                    {
                        item.children // Pokud exisutjí potomci
                        && (
                            <ul className={styles.nestedList}>
                                {
                                    item.children.map((child, childIndex) => renderItem(child, childIndex, depth + 1))
                                }
                            </ul>
                        )
                    }
                </li>
            );
        }
        return (
            <HideableListItem
                title={item.name}
                hiddenByDefault={depth > 1}
                key={index}
                onClick={() => {
                    setSelectedMenuItem({
                        id: item.id, name: item.name, specialType: item.specialType, _links: item._links,
                    });
                    setSelectedRow(undefined);
                }}
                isSelected={selectedMenuItem?.id === item.id}
            >
                <ul className={styles.nestedList}>
                    {item.children?.map((child, childIndex) => renderItem(child, childIndex, depth + 1))}
                </ul>
            </HideableListItem>
        );
    }

    return (
        <ul className={styles.menuItems}>
            {locations.map((item, index) => renderItem(item, index, 0))}
        </ul>
    );
}
