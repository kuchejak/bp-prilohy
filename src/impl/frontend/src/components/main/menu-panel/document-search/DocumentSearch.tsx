import React from "react";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import styles from "./DocumentSearch.module.css";
import Button from "../../../common/button/Button";

/**
 * Komponenta pro vyhlávání dokumentů
 */
export default function DocumentSearch() {
    const [searchText, setSearchText] = React.useState("");

    /**
     * Funkce spouštěná po kliknutí na tlačítko hledat
     */
    const onButtonClick = () => {
        // eslint-disable-next-line no-console
        console.log(`Searching ${searchText}`);
    };

    return (
        <div className={styles.wrapper}>
            <input
                className={styles.input}
                onChange={(ev) => setSearchText(ev.target.value)}
                placeholder="ID dokumentu"
            />
            <Button onClick={onButtonClick} icon={faSearch}>Vyhľadať</Button>
        </div>
    );
}
