import React, { useMemo } from "react";
import TableSection from "../../../common/table-section/TableSection";
import { MainPanelSection, SelectedRowContext } from "../../../../contexts/SelectedRowContext";
import useGetDocuments from "../../../../hooks/documents/useGetDocuments";
import {
    convertDocumentDtoDatesForDisplay,
} from "../../../../util/backend/documents/DocumentsUtils";
import { SelectedMenuItemContext } from "../../../../contexts/SelectedMenuItemContext";

/**
 * Komponenta reprezentující sekci "Dokumenty" v hlavním panelu aplikace
 */
export default function Documents() {
    const { selectedRow, setSelectedRow } = React.useContext(SelectedRowContext);
    const { selectedMenuItem } = React.useContext(SelectedMenuItemContext);

    const documentsUrl = selectedMenuItem?._links?.documents.href;
    if (!documentsUrl) {
        return null;
    }

    const data = useGetDocuments(documentsUrl)
        ?.map((document) => (convertDocumentDtoDatesForDisplay(document)));

    const columns = useMemo(() => [
        {
            Header: "ID",
            accessor: "documentNumber",
            width: 50,
        },
        {
            Header: "Typ",
            accessor: "documentType",
            width: 50,
        },
        {
            Header: "Názov",
            accessor: "title",
            width: 370,
        },
        {
            Header: "Verzia",
            accessor: "version",
            width: 100,
        },
        {
            Header: "Dátum zmeny",
            accessor: "dateOfChange",
            hidden: true,
        },
    ], []);

    return (
        <TableSection
            heading="Dokumenty"
            columns={columns}
            data={data}
            onRowSelect={(rowData) => {
                setSelectedRow({ id: rowData.id, section: MainPanelSection.Document, _links: rowData._links });
            }}
            selectedRowIds={selectedRow?.section === MainPanelSection.Document
                ? { [selectedRow.id]: true }
                : {}}
        />
    );
}
