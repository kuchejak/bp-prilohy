import { render, screen } from "@testing-library/react";
import DowntimesTooltip from "./DowntimesTooltip";

describe("DowntimesTooltip", () => {
    it("should display information about how downtimes are displayed", () => {
        render(<DowntimesTooltip />);
        screen.getByText("Plánované odstávky");
        screen.getByText("Práve prebiehajúce odstávky");
        screen.getByText("Ukončené odstávky");
    });
});
