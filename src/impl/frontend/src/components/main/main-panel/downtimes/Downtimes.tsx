import React, { useMemo } from "react";
import TableSection from "../../../common/table-section/TableSection";
import DowntimesTooltip from "./tooltip/DowntimesTooltip";
import { MainPanelSection, SelectedRowContext } from "../../../../contexts/SelectedRowContext";
import ColorVariant, {
    DowntimesOngoingColor, DowntimesPlannedColor,
} from "../../../../util/display/ColorVariant";
import useGetDowntimes from "../../../../hooks/downtimes/useGetDowntimes";
import formatDate from "../../../../util/display/FormatDate";
import { SelectedMenuItemContext } from "../../../../contexts/SelectedMenuItemContext";

/**
 * Komponenta reprezentující sekci "Odstávky" v hlavním panelu aplikace
 */
export default function Downtimes() {
    const {
        selectedRow,
        setSelectedRow,
    } = React.useContext(SelectedRowContext);

    const { selectedMenuItem } = React.useContext(SelectedMenuItemContext);

    const downtimesUrl = selectedMenuItem?._links?.downtimes.href;
    if (!downtimesUrl) {
        return null;
    }

    const data = useGetDowntimes(downtimesUrl)?.map((downtime) => { // Formátování pro zobrazení
        const now = new Date();
        let rowColor: { color?: ColorVariant } = {};
        if (downtime.from && downtime.to && downtime.from <= now && now <= downtime.to) {
            rowColor = { color: DowntimesOngoingColor };
        } else if (downtime.from && downtime.from >= now) {
            rowColor = { color: DowntimesPlannedColor };
        }
        return {
            ...downtime,
            from: downtime.from ? formatDate(downtime.from) : "",
            to: downtime.to ? formatDate(downtime.to) : "",
            ...rowColor,
        };
    });

    const columns = useMemo(() => [
        {
            Header: "Začiatok",
            accessor: "from",
        },
        {
            Header: "Koniec",
            accessor: "to",
        },
        {
            Header: "Dôvod",
            accessor: "reason",
            width: 250,
        },
        {
            Header: "Umiestnenie",
            accessor: "location",
            hidden: true,
        },
    ], []);

    return (
        <TableSection
            heading="Odstávky"
            tooltip={<DowntimesTooltip />}
            columns={columns}
            data={data}
            onRowSelect={(rowData) => {
                setSelectedRow({
                    id: rowData.id,
                    section: MainPanelSection.Downtime,
                    _links: rowData._links,
                });
            }}
            selectedRowIds={selectedRow?.section === MainPanelSection.Downtime
                ? { [selectedRow.id]: true }
                : {}}
        />
    );
}
