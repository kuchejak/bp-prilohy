import RowInformation from "../../../../common/table-section/row-information/RowInformation";
import {
    DowntimesOngoingColor,
    DowntimesPlannedColor,
    WHITE,
} from "../../../../../util/display/ColorVariant";

/**
 * Tooltip pro tabulku odstávek
 */
export default function DowntimesTooltip() {
    const rows = [
        {
            description: "Plánované odstávky",
            color: DowntimesPlannedColor,
        },
        {
            description: "Práve prebiehajúce odstávky",
            color: DowntimesOngoingColor,
        },
        {
            description: "Ukončené odstávky",
            color: { colorNormal: WHITE, colorSelected: WHITE },
        },
    ];
    return (
        <RowInformation rows={rows} />
    );
}
