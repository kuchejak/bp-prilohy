import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./MainPanel.module.css";
import Downtimes from "./downtimes/Downtimes";
import Documents from "./documents/Documents";
import { SelectedMenuItemContext } from "../../../contexts/SelectedMenuItemContext";
import { LocationType } from "../../../util/backend/locations/LocationsUtils";

/**
 * Vertikální panel obsahujícíc tabulky
 */
export default function MainPanel() {
    const { selectedMenuItem } = React.useContext(SelectedMenuItemContext);
    if (selectedMenuItem) {
        return (
            <div className={styles.mainPanel}>

                <h2>
                    {selectedMenuItem.icon && <FontAwesomeIcon icon={selectedMenuItem.icon} className={styles.icon} />}
                    {selectedMenuItem.name}
                </h2>
                {
                    shouldDisplayDowntimes(selectedMenuItem.specialType)
                    && <Downtimes />
                }
                {
                    shouldDisplayDocuments(selectedMenuItem.specialType)
                    && <Documents />
                }
            </div>
        );
    }
    return null;
}

function shouldDisplayDowntimes(selectedLocationType: LocationType | null) {
    if (!selectedLocationType) return true;
    return ["Home", "SystemRoot"].includes(selectedLocationType);
}

function shouldDisplayDocuments(selectedLocationType: LocationType | null) {
    return selectedLocationType !== "Emails";
}
