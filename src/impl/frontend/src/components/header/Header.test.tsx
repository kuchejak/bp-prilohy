import { render, screen } from "@testing-library/react";
import Header from "./Header";

describe("Header", () => {
    it("should display heading and buttons", () => {
        render(<Header />);
        screen.getByRole("banner");
        const appTitle = screen.getByRole("heading");
        expect(appTitle.textContent).toBe("Prevádzkový portál IS SKP");
        const buttons = screen.getAllByRole("button").map((button) => button.textContent);
        expect(buttons.length).toBe(2);
        expect(buttons).toContain("Nastavenie");
        expect(buttons).toContain("Návod");
    });
});
