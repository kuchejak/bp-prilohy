import { faDownload } from "@fortawesome/free-solid-svg-icons";
import Button from "../../common/button/Button";

interface Props {
    className?: string
}

/**
 * Tlačítko uričené pro stažení návodu.
 * @param className CSS třída, která se má na tlačítko aplikovat.
 */
export default function ManualDownloadButton({ className }: Props) {
    // Prozatím pouze talčítko, které nic nedělá
    return (
        <Button
            icon={faDownload}
            className={className}
        >
            Návod
        </Button>
    );
}
