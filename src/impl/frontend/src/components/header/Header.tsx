import styles from "./Header.module.css";
import ManualDownloadButton from "./manual-download-button/ManualDownloadButton";
import Settings from "./settings/Settings";
import User from "../main/menu-panel/user/User";

/**
 * Záhlaví s nadpisem, informacemi o užiateli, nastavením a tlačítkem pro stažení návodu.
 */
export default function Header() {
    return (
        <header className={`${styles.header} ${styles.headerFlex}`}>
            <div className={styles.headerFlex}>
                <h1>Prevádzkový portál IS SKP</h1>
            </div>
            <div className={styles.headerFlex}>
                <User />
                <Settings />
                <ManualDownloadButton className={styles.downloadButton} />
            </div>
        </header>
    );
}
