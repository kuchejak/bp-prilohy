import React from "react";
import {
    faBook,
    faCogs,
    faEnvelope,
    faPowerOff,
    faUsers,
} from "@fortawesome/free-solid-svg-icons";
import Button from "../../common/button/Button";
import styles from "./Settings.module.css";

/**
 * Nastavení - tlačítka spouštějící různé funkce. Defaultně se zobrazuje jako jedno tlačítko, které se po kliknutí rozšíří
 * a zobrazí i tlačítka další.
 */
export default function Settings() {
    const [isHidden, setIsHidden] = React.useState(true);

    return (
        <div>
            {
                !isHidden
                && (
                    <>
                        <Button
                            onClick={() => window.location.reload()}
                            icon={faPowerOff}
                            className={styles.innerButton}
                        >
                            Reštart
                        </Button>
                        <Button
                            onClick={() => console.log("TODO Správa uživatelů")}
                            icon={faUsers}
                            className={styles.innerButton}
                        >
                            Správa používateľov
                        </Button>
                        <Button
                            onClick={() => console.log("TODO Správa manuálů")}
                            icon={faBook}
                            className={styles.innerButton}
                        >
                            Správa manuálů
                        </Button>
                        <Button
                            onClick={() => console.log("TODO Poslat email administrátorům")}
                            icon={faEnvelope}
                            className={styles.innerButton}
                        >
                            Poslať e-mail administrátorom
                        </Button>
                    </>
                )
            }
            <Button onClick={() => setIsHidden(!isHidden)} icon={faCogs}>
                Nastavenie
            </Button>
        </div>
    );
}
