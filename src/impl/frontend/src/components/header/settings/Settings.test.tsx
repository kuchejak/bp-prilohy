import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Settings from "./Settings";

describe("Settings", () => {
    it("should be displayed as one button by default", () => {
        render(<Settings />);
        const btn = screen.getByRole("button");
        expect(btn.textContent).toBe("Nastavenie");
    });

    it("should expand after user clicks on it", () => {
        render(<Settings />);
        userEvent.click(screen.getByRole("button"));
        const allButtons = screen.getAllByRole("button");
        expect(allButtons.length).toBe(5);
        screen.getByText("Reštart");
        screen.getByText("Správa používateľov");
        screen.getByText("Správa manuálů");
        screen.getByText("Poslať e-mail administrátorom");
        screen.getByText("Nastavenie");
        expect(allButtons[4]?.textContent).toBe("Nastavenie"); // Kontrola umístění na konci
    });

    it("should hide after being clicked on the second time", () => {
        render(<Settings />);
        let btn = screen.getByRole("button");
        userEvent.click(btn);
        userEvent.click(btn);
        btn = screen.getByRole("button");
        expect(btn.textContent).toBe("Nastavenie");
    });
});
