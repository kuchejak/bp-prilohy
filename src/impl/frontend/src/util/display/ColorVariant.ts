export default interface ColorVariant {
    colorNormal: string;
    colorSelected: string;
}

// Barvy používané v tabulkách
export const WHITE = "#ffffff";
export const GREEN_LIGHT = "#e1ffe6";
export const GREEN_DARK = "rgb(87, 113, 96)";
export const YELLOW_LIGHT = "#feffd9";
export const YELLOW_DARK = "rgba(161, 160, 78)";
export const RED_DARK = "rgba(161, 52, 67)";
export const RED_LIGHT = "#ffdcdc";

// Downtimes
export const DowntimesPlannedColor: ColorVariant = { colorNormal: YELLOW_LIGHT, colorSelected: YELLOW_DARK };
export const DowntimesOngoingColor: ColorVariant = { colorNormal: RED_LIGHT, colorSelected: RED_DARK };
