// Formát pro datumy (pozn. používá se cz-CS místo sk-SK, protože slovenský formát přidává před čas čárku, např. "05. 03. 2022, 19:29")
const formatter = new Intl.DateTimeFormat("cz-CS", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
});

/**
 * Naformátuje datum na vstupu tak, aby bylo dobře čitelné uživatelem
 * @param date Datum, které se má naformátovat
 */
export default function formatDate(date: Date) {
    return formatter.format(date);
}
