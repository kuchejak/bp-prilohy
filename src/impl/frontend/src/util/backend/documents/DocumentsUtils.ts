import { DefaultBackendEntity } from "../common/BackendUtils";
import formatDate from "../../display/FormatDate";
import { Location } from "../locations/LocationsUtils";

export interface DocumentDto extends DefaultBackendEntity {
    documentNumber: number,
    documentType: string,
    title: string,
    version: string,
    dateOfChange: Date | null,
    location: string
}

export interface Document extends Omit<DocumentDto, "documentType" | "location"> {
    documentType: {
        extension: string,
        description: string,
    }
    accessPermission: string,
    reasonForInsertion: string,
    location: Location
}

export interface DocumentDtosEmbedded {
    _embedded?: {
        documentDtoList: DocumentDto[];
    }
}

export enum DocumentsParams {
    VERSION = "version",

}

export enum VersionType {
    ALL = "all",
    LATEST = "latest",
}

export function convertDocumentDtoDatesForDisplay(documentDto: DocumentDto) {
    return {
        ...documentDto,
        dateOfChange: documentDto.dateOfChange ? formatDate(documentDto.dateOfChange) : "",
    };
}
