// Typy pro data přijímaná z backendu
/**
 * Enum s definicemi endpointů
 */
export const enum BackendMapping {
    DOWNTIME = "downtime",
    DOWNTIMES = "downtimes",
    DOCUMENT = "document",
    DOCUMENTS = "documents",
    LOCATIONS = "locations",
}

export interface Link {
    href: string;
}

export interface Links {
    [key: string]: Link
}

export interface DefaultBackendEntity {
    id: number,
    _links?: Links
}

export interface UrlProps {
    entityUrl: string;
}
