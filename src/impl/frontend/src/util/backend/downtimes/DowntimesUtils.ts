import { DefaultBackendEntity } from "../common/BackendUtils";
import { Location } from "../locations/LocationsUtils";

export interface DowntimeDto extends DefaultBackendEntity {
    from: Date | null,
    to: Date | null,
    reason: string,
    location: string
}

export interface Downtime extends Omit<DowntimeDto, "location"> {
    location: Location,
    downtimeNumber: string,
    additionalInfo: string,
    accessPermission: string
}

export interface DowntimeDtosEmbedded {
    _embedded?: {
        downtimeDtoList: DowntimeDto[];
    }
}

export enum DowntimesParams {

}
