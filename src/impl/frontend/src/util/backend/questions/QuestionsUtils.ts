import { DefaultBackendEntity } from "../common/BackendUtils";

export interface QuestionWithAnswer extends DefaultBackendEntity {
    question: string,
    answer: string
}

export interface QuestionsWithAnswersEmbedded {
    _embedded?: {
        questionWithAnswerList: QuestionWithAnswer[];
    }
}
