import { DefaultBackendEntity } from "../common/BackendUtils";

export type LocationType = "Home" | "Technology" | "Info" | "SystemRoot" | "Emails" | null;

export interface LocationDto extends DefaultBackendEntity {
    name: string,
    specialType: LocationType,
    children: LocationDto[]
}

export interface Location extends DefaultBackendEntity {
    system?: string,
    subsystem?: string,
    userWebApplication?: string,
    adminWebApplication?: string,
    channel?: string
}

export interface LocationsEmbedded {
    _embedded?: {
        entityModelLocationTreeDtoList: LocationDto[]
    }
}
