import { createContext } from "react";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { LocationType } from "../util/backend/locations/LocationsUtils";
import { Links } from "../util/backend/common/BackendUtils";

export interface MenuItem {
    id: number,
    name: string;
    specialType: LocationType;
    icon?: IconProp;
    _links?: Links,
}

interface MenuContextValue {
    selectedMenuItem?: MenuItem,
    setSelectedMenuItem: (menuItem: MenuItem | undefined) => void
}

/**
 * Kontext s informacemi o vybrané položce v menu
 */
export const SelectedMenuItemContext = createContext<MenuContextValue>(
    { setSelectedMenuItem: () => {} },
);
