import { createContext } from "react";
import { Links } from "../util/backend/common/BackendUtils";

export enum MainPanelSection {
    Downtime = "Downtime",
    Document = "Document",
}

export interface RowContext {
    id: number,
    section: MainPanelSection,
    _links?: Links,
}

interface SelectedRowContextValue {
    selectedRow?: RowContext;
    setSelectedRow: (row?: RowContext) => void;
}

/**
 * Kontext s informace o vybraném řádku v tabulkách
 */
export const SelectedRowContext = createContext<SelectedRowContextValue>(
    {
        setSelectedRow: () => { },
    },
);
