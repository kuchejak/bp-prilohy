import { IconProp } from "@fortawesome/fontawesome-svg-core";
import {
    faCogs, faFileAlt, faHome, faSitemap,
} from "@fortawesome/free-solid-svg-icons";
import { LocationDto, LocationsEmbedded, LocationType } from "../../util/backend/locations/LocationsUtils";
import { BackendMapping, DefaultBackendEntity } from "../../util/backend/common/BackendUtils";
import { MenuItem } from "../../contexts/SelectedMenuItemContext";
import useBackendGet from "../common/backend/useBackendGet";

export interface LocationWithIcon extends MenuItem, DefaultBackendEntity {
   children: LocationWithIcon[];
}

/**
 * Hook, který z backendu získává informace o lokacích pro zobrazení v menu
 */
export default function useGetLocationsForMenu(): LocationWithIcon[] {
    const request = `${process.env.REACT_APP_BACKEND_URL}/${BackendMapping.LOCATIONS}`;
    const locationsDtos = useBackendGet<LocationsEmbedded>(request);
    if (!locationsDtos) {
        return [];
    }
    return locationsDtos?._embedded?.entityModelLocationTreeDtoList.map(convertLocationDtoToLocationWithIcon) || [];
}

function convertLocationDtoToLocationWithIcon(locationDto: LocationDto): LocationWithIcon {
    return {
        id: locationDto.id,
        name: locationDto.name,
        icon: getIcon(locationDto.specialType),
        children: locationDto.children?.map(convertLocationDtoToLocationWithIcon),
        _links: locationDto._links,
        specialType: locationDto.specialType,
    };
}

function getIcon(specialType: LocationType): IconProp | undefined {
    let result: IconProp | undefined;
    switch (specialType) {
    case "Home": {
        result = faHome;
        break;
    }
    case "Technology": {
        result = faCogs;
        break;
    }
    case "Info": {
        result = faFileAlt;
        break;
    }
    case "SystemRoot": {
        result = faSitemap;
        break;
    }
    default:
    }
    return result;
}
