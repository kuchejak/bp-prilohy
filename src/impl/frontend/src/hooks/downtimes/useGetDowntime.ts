import useBackendGet from "../common/backend/useBackendGet";
import { Downtime } from "../../util/backend/downtimes/DowntimesUtils";
import formatDate from "../../util/display/FormatDate";

/**
 * Hook zajišťující zíkání jednoho dokumentu
 * @param url URL, ze kterého se má získat daná odstávka
 */
export default function useGetDowntime(url: RequestInfo) {
    const downtime = useBackendGet<Downtime>(url);
    if (downtime) {
        return {
            ...downtime,
            from: downtime.from ? formatDate(new Date(downtime.from)) : null,
            to: downtime.to ? formatDate(new Date(downtime.to)) : null,
        };
    }
    return null;
}
