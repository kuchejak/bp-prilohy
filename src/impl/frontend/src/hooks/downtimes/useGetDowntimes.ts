import {
    DowntimeDtosEmbedded,
} from "../../util/backend/downtimes/DowntimesUtils";
import useBackendGet from "../common/backend/useBackendGet";

/**
 * Hook pro fetchování odstávek z backendu. Pokud není uvedena stránka nebo její velikost (nebo je jedna z nich 0),
 * volá se bez parametrů
 * @param downtimesUrl Url pro získání daných odstávek
 */
export default function useGetDowntimes(downtimesUrl: string) {
    const downtimeDtosEmbedded = useBackendGet<DowntimeDtosEmbedded>(downtimesUrl);
    if (!downtimeDtosEmbedded) {
        return [];
    }
    return downtimeDtosEmbedded?._embedded?.downtimeDtoList
        .map((downtime) => (
            {
                ...downtime,
                from: downtime.from ? new Date(downtime.from) : null,
                to: downtime.to ? new Date(downtime.to) : null,
            }
        )) || [];
}
