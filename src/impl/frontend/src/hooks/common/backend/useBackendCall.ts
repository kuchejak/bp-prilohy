import React from "react";

/**
 * Hook pro fetchování dat z backendu.
 * @param requestInfo První arugmet funkce fetch, například url (string)
 * @param requestInit Druhý arugment funkce fetch, například {method: GET}
 * @return Získaná data převedená do JSONu, nebo null, pokud se fetch nebo převod do JSONu nepodaří
 */
export default function useBackendCall<T>(requestInfo: RequestInfo, requestInit: RequestInit): T | null {
    const [fetched, setFetched] = React.useState<T | null>(null);

    const fetchData = async () => {
        try {
            const res = await fetch(requestInfo, requestInit);
            return await res.json();
        } catch (err) {
            return null;
        }
    };

    React.useEffect(() => {
        const effectFunction = async () => { // Používá se vnitřní funkce, protože arugment useEffect nemůže být async
            const result = await fetchData();
            setFetched(result);
        };
        effectFunction();
    }, [requestInfo]); // na DEPS zde záleží, jak často se celá metoda zavolá (takto se zavolá pouze při prvním renderu)

    return React.useMemo(() => fetched, [fetched]);
}
