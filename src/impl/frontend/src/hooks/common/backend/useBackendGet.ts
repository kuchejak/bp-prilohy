import useBackendCall from "./useBackendCall";

/**
 * Hook pro fetchování dat z backendu s použitím HTTP GET.
 * @param requestInfo První arugmet funkce fetch, například url (string)
 */
export default function useBackendGet<T>(requestInfo: RequestInfo): T | null {
    return useBackendCall<T>(requestInfo, { method: "GET" });
}
