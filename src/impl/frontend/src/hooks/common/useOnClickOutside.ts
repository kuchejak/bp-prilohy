import React, { useEffect } from "react";

/**
 * Hook, který provede dodanou funkci po té, co uživatel klikne mimo element určený referencí
 * @param ref Reference na element, mimo který chceme sledovat kliknutí
 * @param onClick Funkce, která se má po kliknutí mimo element provést
 */
export default function useOnClickOutside(ref: React.RefObject<any>, onClick: () => void) {
    useEffect(() => {
        function handleClickOutside(event: MouseEvent) {
            if (ref.current && !ref.current.contains(event.target)) {
                onClick();
            }
        }

        // Bind event listene
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref, onClick]);
}
