import {
    DocumentDtosEmbedded,
} from "../../util/backend/documents/DocumentsUtils";
import useBackendGet from "../common/backend/useBackendGet";

/**
 * Hook pro získávání dokumentů z backendu.
 * @param documentsUrl link na požadované dokumenty
 */
export default function useGetDocuments(documentsUrl: string) {
    const documentDtos = useBackendGet<DocumentDtosEmbedded>(documentsUrl);

    if (!documentDtos) {
        return [];
    }

    return documentDtos?._embedded?.documentDtoList
        .map((document) => (
            {
                ...document,
                dateOfChange: document.dateOfChange ? new Date(document.dateOfChange) : null,
            }
        )) || [];
}
