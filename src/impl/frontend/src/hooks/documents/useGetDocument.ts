import useBackendGet from "../common/backend/useBackendGet";
import { Document } from "../../util/backend/documents/DocumentsUtils";
import formatDate from "../../util/display/FormatDate";

/**
 * Hook zajišťující zíkání jednoho dokumentu
 * @param url URL, ze kterého se má získat daný dokument
 */
export default function useGetDocument(url: RequestInfo) {
    const document = useBackendGet<Document>(url);
    if (document) {
        return {
            ...document,
            dateOfChange: document.dateOfChange ? formatDate(new Date(document.dateOfChange)) : null,
        };
    }
    return null;
}
