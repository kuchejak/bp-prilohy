package cz.syntea.app.iportal.domain.question.controller;

import cz.syntea.app.iportal.domain.app.IPortalApp;
import cz.syntea.app.iportal.domain.question.model.QuestionWithAnswer;
import cz.syntea.app.iportal.domain.question.service.QuestionService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

@SpringBootTest(classes = IPortalApp.class)
@AutoConfigureMockMvc
class QuestionControllerTest {

    private final QuestionWithAnswer questionWithAnswer = new QuestionWithAnswer(
            1,
            1,
            "Question?",
            "Answer"
    );

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private QuestionService questionService;

    @Test
    void getAll_ok() throws Exception {
        int idEntity = 1;
        BDDMockito.given(questionService.findAll(idEntity)).willReturn(Collections.singletonList(questionWithAnswer));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/questions?idEntity={idEntity}", idEntity))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.questionWithAnswerList[0].id", CoreMatchers.is((int) questionWithAnswer.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.questionWithAnswerList[0].answer", CoreMatchers.is(questionWithAnswer.getAnswer())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.questionWithAnswerList[0].question", CoreMatchers.is(questionWithAnswer.getQuestion())));

        BDDMockito.verify(questionService, Mockito.atLeastOnce()).findAll(idEntity);

    }
}
