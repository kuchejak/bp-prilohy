package cz.syntea.app.iportal.domain.location.controller;

import cz.syntea.app.iportal.domain.app.IPortalApp;
import cz.syntea.app.iportal.domain.location.def.LocationType;
import cz.syntea.app.iportal.domain.location.dto.LocationTreeDto;
import cz.syntea.app.iportal.domain.location.service.LocationService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

@SpringBootTest(classes = IPortalApp.class)
@AutoConfigureMockMvc
class LocationControllerTest {

    private final LocationTreeDto locationTreeDto = new LocationTreeDto(
            1,
            "Nazev",
            LocationType.SystemRoot,
            null
    );

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LocationService locationService;

    @Test
    void getAll_ok() throws Exception {
        BDDMockito.given(locationService.findAll()).willReturn(Collections.singletonList(locationTreeDto));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/locations"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.entityModelLocationTreeDtoList[0].id", CoreMatchers.is((int) locationTreeDto.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.entityModelLocationTreeDtoList[0].name", CoreMatchers.is(locationTreeDto.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.entityModelLocationTreeDtoList[0].specialType", CoreMatchers.is(locationTreeDto.getSpecialType().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.entityModelLocationTreeDtoList[0].children", CoreMatchers.is(locationTreeDto.getChildren())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.entityModelLocationTreeDtoList[0]._links.documents.href", CoreMatchers.containsString("documents")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.entityModelLocationTreeDtoList[0]._links.downtimes.href", CoreMatchers.containsString("downtimes")));

        BDDMockito.verify(locationService, Mockito.atLeastOnce()).findAll();
    }
}
