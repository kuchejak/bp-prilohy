package cz.syntea.app.iportal.domain.downtime.controller;

import cz.syntea.app.iportal.domain.app.IPortalApp;
import cz.syntea.app.iportal.domain.common.def.AccessPermission;
import cz.syntea.app.iportal.domain.downtime.dto.DowntimeDto;
import cz.syntea.app.iportal.domain.downtime.model.Downtime;
import cz.syntea.app.iportal.domain.downtime.service.DowntimeService;
import cz.syntea.app.iportal.domain.location.def.LocationType;
import cz.syntea.app.iportal.domain.location.model.Location;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

@SpringBootTest(classes = IPortalApp.class)
@AutoConfigureMockMvc
class DowntimeControllerTest {

    private final DowntimeDto downtimeDto = new DowntimeDto(
            1,
            1,
            LocalDateTime.MAX,
            LocalDateTime.MAX,
            "Reason",
            "a/b/c"
    );

    private final Downtime downtime = new Downtime(
            1,
            "123/123",
            LocalDateTime.MAX,
            LocalDateTime.MAX,
            "Reason",
            "Additional info",
            AccessPermission.ALL,
            new Location(1, "DKP", null, null, null, null, LocationType.Technology)
    );

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DowntimeService downtimeService;

    @Test
    void getAll_ok() throws Exception {
        int idLocation = 1;
        int page = 1;
        int size = 10;
        BDDMockito.given(downtimeService.findAll(idLocation, page, size)).willReturn(
                Collections.singletonList(downtimeDto)
        );

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/downtimes?page={page}&size={size}&idLocation={idLocation}",
                page, size, idLocation))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.downtimeDtoList[0].id", CoreMatchers.is((int) downtimeDto.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.downtimeDtoList[0].from", CoreMatchers.is(downtimeDto.getFrom().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.downtimeDtoList[0].to", CoreMatchers.is(downtimeDto.getTo().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.downtimeDtoList[0].reason", CoreMatchers.is(downtimeDto.getReason())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.downtimeDtoList[0].location", CoreMatchers.is(downtimeDto.getLocation())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.downtimeDtoList[0]._links.self.href", CoreMatchers.containsString("downtimes/" + downtimeDto.getId())));

        BDDMockito.verify(downtimeService, Mockito.atLeastOnce()).findAll(idLocation, page, size);
    }

    @Test
    void getById_ok() throws Exception {
        int idDown = 1;

        BDDMockito.given(downtimeService.findById(idDown)).willReturn(Optional.of(downtime));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/v1/downtimes/{idDown}", idDown)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is((int) downtime.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.downtimeNumber", CoreMatchers.is(downtime.getDowntimeNumber())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.from", CoreMatchers.is(downtime.getFrom().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.to", CoreMatchers.is(downtime.getTo().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.reason", CoreMatchers.is(downtime.getReason())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.additionalInfo", CoreMatchers.is(downtime.getAdditionalInfo())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.accessPermission", CoreMatchers.is(downtime.getAccessPermission().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.accessPermission", CoreMatchers.is(downtime.getAccessPermission().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.location.id", CoreMatchers.is((int) downtime.getLocation().getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.location.system", CoreMatchers.is(downtime.getLocation().getSystem())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.location.specialType", CoreMatchers.is(downtime.getLocation().getSpecialType().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.questions.href", CoreMatchers.containsString("questions")));

        BDDMockito.verify(downtimeService, Mockito.atLeastOnce()).findById(idDown);
    }
}
