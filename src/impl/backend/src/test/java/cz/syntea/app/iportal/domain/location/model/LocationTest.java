package cz.syntea.app.iportal.domain.location.model;

import cz.syntea.app.iportal.domain.location.def.LocationType;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LocationTest {

    @ParameterizedTest
    @CsvSource({
            "DKP,,,,,DKP",
            "DKP,EEE,,,,DKP/EEE",
            "DKP,EEE,DDD,,,DKP/EEE/DDD",
            "DKP,EEE,DDD,AAA,,DKP/EEE/DDD/AAA",
            "DKP,EEE,DDD,,BBB,DKP/EEE/DDD/BBB",
            "DKP,EEE,DDD,'',BBB,DKP/EEE/DDD/BBB",
    })
    public void testGetPath_ok(String system, String subsystem, String userWebApp, String adminWebApp, String channel, String result) {
        Location location = new Location(1, system, subsystem, userWebApp, adminWebApp, channel, null);
        assertEquals(result, location.getPath());
    }

    @ParameterizedTest
    @EnumSource(LocationType.class)
    public void testGetPathSpecialType_ok(LocationType locationType) {
        Location location = new Location(1, "DDD", "AAA", null, "BBB", "CCC", locationType);
        assertEquals("", location.getPath());
    }
}
