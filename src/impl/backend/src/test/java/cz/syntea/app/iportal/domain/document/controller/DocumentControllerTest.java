package cz.syntea.app.iportal.domain.document.controller;

import cz.syntea.app.iportal.domain.app.IPortalApp;
import cz.syntea.app.iportal.domain.common.def.AccessPermission;
import cz.syntea.app.iportal.domain.document.def.DocumentType;
import cz.syntea.app.iportal.domain.document.def.VersionType;
import cz.syntea.app.iportal.domain.document.dto.DocumentDto;
import cz.syntea.app.iportal.domain.document.model.Document;
import cz.syntea.app.iportal.domain.document.service.DocumentService;
import cz.syntea.app.iportal.domain.location.def.LocationType;
import cz.syntea.app.iportal.domain.location.model.Location;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

@SpringBootTest(classes = IPortalApp.class)
@AutoConfigureMockMvc
class DocumentControllerTest {

    private final DocumentDto documentDto = new DocumentDto(1,
            1,
            1,
            DocumentType.AP,
            "Title",
            "1.0",
            LocalDateTime.MAX,
            "a/b/c");

    private final Document document = new Document(
            1,
            1,
            "Title",
            DocumentType.AP,
            "1.0",
            "Test reason",
            LocalDateTime.MAX,
            AccessPermission.ALL,
            new Location(1, "DKP", null, null, null, null, LocationType.Home)
    );

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DocumentService documentService;

    @Test
    void getAll_ok() throws Exception {
        VersionType versionType = VersionType.LATEST;
        int idLocation = 1;
        int page = 1;
        int size = 10;

        BDDMockito.given(documentService.findAll(versionType, idLocation, page, size)).willReturn(
                Collections.singletonList(documentDto)
        );

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/v1/documents?page={page}&size={size}&version={versionType}&idLocation={idLocation}",
                                page, size, versionType.getValue(), idLocation)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0].id", CoreMatchers.is((int) documentDto.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0].documentNumber", CoreMatchers.is((int) documentDto.getDocumentNumber())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0].documentType", CoreMatchers.is(documentDto.getDocumentType().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0].title", CoreMatchers.is(documentDto.getTitle())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0].version", CoreMatchers.is(documentDto.getVersion())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0].dateOfChange", CoreMatchers.is(documentDto.getDateOfChange().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0].location", CoreMatchers.is(documentDto.getLocation())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._embedded.documentDtoList[0]._links.self.href", CoreMatchers.containsString("documents/" + documentDto.getId())));

        BDDMockito.verify(documentService, Mockito.atLeastOnce()).findAll(versionType, idLocation, page, size);
    }

    @Test
    void getById_ok() throws Exception {
        int idDoc = 1;

        BDDMockito.given(documentService.findById(idDoc)).willReturn(Optional.of(document));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/v1/documents/{idDoc}", idDoc)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is((int) document.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.documentNumber", CoreMatchers.is((int) document.getDocumentNumber())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.title", CoreMatchers.is(document.getTitle())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.documentType.extension", CoreMatchers.is(document.getDocumentType().getExtension().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.documentType.description", CoreMatchers.is(document.getDocumentType().getDescription())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.version", CoreMatchers.is(document.getVersion())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.reasonForInsertion", CoreMatchers.is(document.getReasonForInsertion())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dateOfChange", CoreMatchers.is(document.getDateOfChange().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.accessPermission", CoreMatchers.is(document.getAccessPermission().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.location.id", CoreMatchers.is((int) document.getLocation().getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.location.system", CoreMatchers.is(document.getLocation().getSystem())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.location.specialType", CoreMatchers.is(document.getLocation().getSpecialType().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.questions.href", CoreMatchers.containsString("questions")));

        BDDMockito.verify(documentService, Mockito.atLeastOnce()).findById(idDoc);
    }
}
