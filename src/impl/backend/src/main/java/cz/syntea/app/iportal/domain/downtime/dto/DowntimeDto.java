package cz.syntea.app.iportal.domain.downtime.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * Třída reprezentující základní informace o odstávce
 * @author kuchejda
 * @since 2022-03-27
 */
@Value
public class DowntimeDto {
    long id;
    @JsonIgnore long idLocation; // Na FE s v případě DTO posílá jako string
    LocalDateTime from;
    LocalDateTime to;
    String reason;
    String location;
}
