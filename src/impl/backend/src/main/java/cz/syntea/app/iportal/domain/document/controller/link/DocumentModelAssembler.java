package cz.syntea.app.iportal.domain.document.controller.link;

import cz.syntea.app.iportal.domain.document.controller.DocumentController;
import cz.syntea.app.iportal.domain.document.def.VersionType;
import cz.syntea.app.iportal.domain.document.model.Document;
import cz.syntea.app.iportal.domain.question.controller.QuestionController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * Třída přidávající linky k Dokumentům
 * @author kuchejda
 * @since 2022-04-06
 */
@Component
public class DocumentModelAssembler implements RepresentationModelAssembler<Document, EntityModel<Document>> {

    @Override
    @NonNull
    public EntityModel<Document> toModel(@NonNull Document document) {
        return EntityModel.of(document,
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DocumentController.class).getById(document.getId())).withSelfRel(),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DocumentController.class).getAll(
                        VersionType.LATEST.getValue(),
                        document.getLocation().getId(),
                        1,
                        10)).withRel("documents"),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(QuestionController.class).getAll(document.getId())).withRel("questions")
        );
    }

}
