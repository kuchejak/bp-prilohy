package cz.syntea.app.iportal.domain.downtime.model;

import cz.syntea.app.iportal.domain.common.def.AccessPermission;
import cz.syntea.app.iportal.domain.location.model.Location;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * Třída reprezentující odstávku
 * @author kuchejda
 * @since 2022-05-03
 */
@Value
public class Downtime {
    long id;
    String downtimeNumber;
    LocalDateTime from;
    LocalDateTime to;
    String reason;
    String additionalInfo;
    AccessPermission accessPermission;
    Location location;
}
