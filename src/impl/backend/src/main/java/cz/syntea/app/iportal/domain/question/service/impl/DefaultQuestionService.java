package cz.syntea.app.iportal.domain.question.service.impl;

import com.github.javafaker.Faker;
import cz.syntea.app.iportal.domain.question.model.QuestionWithAnswer;
import cz.syntea.app.iportal.domain.question.service.QuestionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Defaultní servica pro práci s otázkami
 * @author kuchejda
 * @since 2022-05-08
 */
@Service
public class DefaultQuestionService implements QuestionService {

    private final Faker faker = new Faker(new Locale("sk"));

    @Override
    public List<QuestionWithAnswer> findAll(long idEntity) {
        return generateSampleQuestionsAndAnswers(idEntity);
    }

    private List<QuestionWithAnswer> generateSampleQuestionsAndAnswers(long idEntity) {
        int count = faker.number().numberBetween(0, 4);
        ArrayList<QuestionWithAnswer> result = new ArrayList<>(count);
        for (int i = 0; i< count; i++) {
            String question = faker.shakespeare().kingRichardIIIQuote();
            question = question.substring(0, question.length() - 1) + "?"; // Vymění poslední znak za otazník
            String answer = faker.shakespeare().kingRichardIIIQuote();
           result.add(new QuestionWithAnswer(
                   faker.number().numberBetween(1, 10000),
                   idEntity,
                   question,
                   answer
           ));
        }
        return result;
    }
}
