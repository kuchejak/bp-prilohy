package cz.syntea.app.iportal.domain.location.service.impl;

import com.github.javafaker.Faker;
import cz.syntea.app.iportal.domain.location.def.LocationType;
import cz.syntea.app.iportal.domain.location.dto.LocationTreeDto;
import cz.syntea.app.iportal.domain.location.model.Location;
import cz.syntea.app.iportal.domain.location.service.LocationService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Defaultnís servica pro práci s lokacemi
 * @author kuchejda
 * @since 2022-04-27
 */
@Service
public class DefaultLocationService implements LocationService {

    private final Faker faker = new Faker(new Locale("sk"));

    @Override
    public Optional<Location> findById(long id) {
        return Optional.of(generateSampleLocation(id));
    }

    @Override
    public List<LocationTreeDto> findAll() {

        return Arrays.asList(
                    new LocationTreeDto(1, "Domov", LocationType.Home, null),
                    new LocationTreeDto(2, "Technológia", LocationType.Technology, null),
                    new LocationTreeDto(3, "Všeobecné informácie", LocationType.Info,
                            Collections.singletonList(new LocationTreeDto(4, "Zaslané emaily", LocationType.Emails, null))),
                    new LocationTreeDto(5, "IS SKP", LocationType.SystemRoot,
                            Arrays.asList(
                                    new LocationTreeDto(6, "DKP", null,
                                            Arrays.asList(
                                                    new LocationTreeDto(7, "RL2GET", null, null),
                                                    new LocationTreeDto(8, "RL5GET", null, null),
                                                    new LocationTreeDto(9, "RNGET", null,
                                                            Collections.singletonList(new LocationTreeDto(130, "R222", null, null))),
                                                    new LocationTreeDto(10, "RV2GET", null, null),
                                                    new LocationTreeDto(11, "RV9IA", null, null)
                                            )),
                                    new LocationTreeDto(12, "EVI", null,
                                            Collections.singletonList(
                                                    new LocationTreeDto(13, "EVAADMIN", null, null)
                                            )
                            ))
                )
        );
    }

    Location generateSampleLocation(long id) {
        String system = faker.app().name();
        String subsystem = faker.app().name();
        String userWebApp = faker.app().name();
        String adminWebApp = faker.app().name();
        String channel = faker.app().name();
        return new Location(id,
                system,
                faker.bool().bool() ? subsystem : null,
                faker.bool().bool() ? userWebApp : null,
                faker.bool().bool() ? adminWebApp : null,
                faker.bool().bool() ? channel : null,
                null);
    }

}
