package cz.syntea.app.iportal.domain.question.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Value;

/**
 * Třída reprezentující otázku s odpovědí
 * @author kuchejda
 * @since 2022-05-08
 */
@Value
public class QuestionWithAnswer {
    long id;
    @JsonIgnore long idEntity; // Id odtávky nebo dokumentu
    String question;
    String answer;
}
