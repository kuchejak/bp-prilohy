package cz.syntea.app.iportal.domain.location.controller.dto;

import cz.syntea.app.iportal.domain.location.def.LocationType;
import cz.syntea.app.iportal.domain.location.dto.LocationTreeDto;
import lombok.Value;
import org.springframework.hateoas.EntityModel;

import java.util.List;

/**
 * Třída paralelní k {@link LocationTreeDto}, která umožňuje ukládat potomky jako obalené v EntityModel (je potřeba pro
 * přidání linků u controllerů)
 * @author kuchejda
 * @since 2022-04-27
 */
@Value
public class EntityModelLocationTreeDto {
    long id;
    String name;
    LocationType specialType;
    List<EntityModel<EntityModelLocationTreeDto>> children;
}
