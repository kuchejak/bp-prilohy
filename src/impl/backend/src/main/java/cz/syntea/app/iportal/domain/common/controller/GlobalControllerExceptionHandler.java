package cz.syntea.app.iportal.domain.common.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;

/**
 * Globální error handling pro REST API controllery
 * @author kuchejda
 * @since 2022-04-04
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<?> handleInvalidArgument(ResponseStatusException exception, WebRequest webRequest) {
        throw exception; // Zde je případně možné globálně upravit handling
    }
}
