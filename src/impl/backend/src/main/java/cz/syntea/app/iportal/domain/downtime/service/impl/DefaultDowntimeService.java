package cz.syntea.app.iportal.domain.downtime.service.impl;

import com.github.javafaker.Faker;
import cz.syntea.app.iportal.domain.common.def.AccessPermission;
import cz.syntea.app.iportal.domain.downtime.dto.DowntimeDto;
import cz.syntea.app.iportal.domain.downtime.model.Downtime;
import cz.syntea.app.iportal.domain.downtime.service.DowntimeService;
import cz.syntea.app.iportal.domain.location.model.Location;
import cz.syntea.app.iportal.domain.location.service.LocationService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Defaultní servisa pro práci s odstávkami
 * @author kuchejda
 * @since 2022-03-27
 */
@Service
public class DefaultDowntimeService implements DowntimeService {

    private final Faker faker = new Faker(new Locale("sk"));

    private final LocationService locationService;

    public DefaultDowntimeService(LocationService locationService) {
        this.locationService = locationService;
    }

    public List<DowntimeDto> findAll(long idLocation, int page, int size) {
        List<DowntimeDto> result = generateSampleDowntimes(size);
        result.sort((a, b) -> b.getFrom().compareTo(a.getFrom()));
        return result;
    }

    @Override
    public Optional<Downtime> findById(long id) {
        return Optional.of(generateSampleDowntime(id));
    }

    private List<DowntimeDto> generateSampleDowntimes(int count) {
        List<DowntimeDto> result = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            result.add(generateSampleDowntimeDto(i));
        }
        return result;
    }

    private DowntimeDto generateSampleDowntimeDto(long id) {
        // from a to
        LocalDateTime from = faker.date().between(faker.date().past(5, TimeUnit.DAYS), faker.date().future(5, TimeUnit.DAYS))
                .toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime to = faker.date().between(faker.date().past(5, TimeUnit.DAYS), faker.date().future(5, TimeUnit.DAYS))
                .toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        if (from.compareTo(to) > 0) {
            LocalDateTime tmp = from;
            from = to;
            to = tmp;
        }

        return new DowntimeDto(id,
                faker.number().numberBetween(1, 99999),
                from,
                to,
                faker.book().title(),
                faker.lorem().word() + "/" + faker.lorem().word());

    }

    private Downtime generateSampleDowntime(long id) {

        DowntimeDto downtimeDto = generateSampleDowntimeDto(id);
        String additionalInfo = faker.shakespeare().romeoAndJulietQuote();
        String downtimeNumber = faker.number().numberBetween(2010, 2022) + "/" + faker.number().numberBetween(1,99);
        Location location = locationService.findById(faker.number().numberBetween(1, 10000)).get();

        return new Downtime(id,
                downtimeNumber,
                downtimeDto.getFrom(),
                downtimeDto.getTo(),
                downtimeDto.getReason(),
                additionalInfo,
                faker.bool().bool() ? AccessPermission.ALL : AccessPermission.SKP_ONLY,
                location
        );
    }
}
