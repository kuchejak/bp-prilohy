package cz.syntea.app.iportal.domain.common;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author kuchejda
 * @since 2022-03-26
 */
@Configuration
@ComponentScan
public class CommonConfig {
}
