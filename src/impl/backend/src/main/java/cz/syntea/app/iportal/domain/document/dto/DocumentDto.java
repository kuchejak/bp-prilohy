package cz.syntea.app.iportal.domain.document.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.syntea.app.iportal.domain.document.def.DocumentType;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * Třída reprezentující základní informace o dokumentu
 * @author kuchejda
 * @since 2022-04-06
 */
@Value
public class DocumentDto {
    long id;
    long documentNumber;
    @JsonIgnore long idLocation; // Na FE s v případě DTO posílá jako string
    DocumentType documentType;
    String title;
    String version;
    LocalDateTime dateOfChange;
    String location;

    // Pro DTO stačí v JSONu zkratka typu dokumentu
    @JsonGetter("documentType")
    String getDocumentTypeShort() {
        return documentType.name();
    }
}
