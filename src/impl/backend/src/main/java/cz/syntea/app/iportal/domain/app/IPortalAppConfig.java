package cz.syntea.app.iportal.domain.app;

import cz.syntea.app.iportal.domain.common.CommonConfig;
import cz.syntea.app.iportal.domain.document.DocumentConfig;
import cz.syntea.app.iportal.domain.downtime.DowntimeConfig;
import cz.syntea.app.iportal.domain.location.LocationConfig;
import cz.syntea.app.iportal.domain.question.QuestionConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.config.EnableHypermediaSupport;

/**
 * Konfigurace celé aplikace
 * @author kuchejda
 * @since 2022-03-25
 */
@Configuration
@Import({
        DowntimeConfig.class,
        DocumentConfig.class,
        CommonConfig.class,
        LocationConfig.class,
        QuestionConfig.class
})
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class IPortalAppConfig {
}
