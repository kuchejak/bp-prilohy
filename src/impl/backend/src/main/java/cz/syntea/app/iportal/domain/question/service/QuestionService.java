package cz.syntea.app.iportal.domain.question.service;

import cz.syntea.app.iportal.domain.question.model.QuestionWithAnswer;

import java.util.List;

/**
 * Metody pro práci s otázkami a odpověďmi
 * @author kuchejda
 * @since 2022-05-08
 */
public interface QuestionService {
    /**
     * Vrátí všechny otázky a odpovědi pro danou lokaci
     * @param idEntity Id entity (odstávky nebo dokumentu), ke které se dotaz váže
     * @return Všechny otázky a odpovědi pro danou lokaci
     */
    List<QuestionWithAnswer> findAll(long idEntity);
}
