package cz.syntea.app.iportal.domain.location.controller.link;

import cz.syntea.app.iportal.domain.document.controller.DocumentController;
import cz.syntea.app.iportal.domain.document.def.VersionType;
import cz.syntea.app.iportal.domain.downtime.controller.DowntimeController;
import cz.syntea.app.iportal.domain.location.controller.LocationController;
import cz.syntea.app.iportal.domain.location.controller.dto.EntityModelLocationTreeDto;
import cz.syntea.app.iportal.domain.location.dto.LocationTreeDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Třída přidávající linky k {@link LocationTreeDto}, výstupem je {@link EntityModelLocationTreeDto}
 * @author kuchejda
 * @since 2022-04-27
 */
@Component
public class LocationDtoTreeAssembler implements RepresentationModelAssembler<LocationTreeDto, EntityModel<EntityModelLocationTreeDto>> {

    @Override
    @NonNull
    public EntityModel<EntityModelLocationTreeDto> toModel(@NonNull LocationTreeDto locationTreeDto) {

        // Převod se provádí rekurzivně i pro všechny potomky
        List<LocationTreeDto> children = locationTreeDto.getChildren();
        EntityModelLocationTreeDto result = new EntityModelLocationTreeDto(
                locationTreeDto.getId(),
                locationTreeDto.getName(),
                locationTreeDto.getSpecialType(),
                children == null ? null : children.stream().map(this::toModel).collect(Collectors.toList())
        );

        return EntityModel.of(result,
                //WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(LocationController.class).getById(locationTreeDto.getId())).withSelfRel(), TODO dodělat endpoit
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(LocationController.class).getAll()).withRel("locations"),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DowntimeController.class).getAll(result.getId(), 1, 10)).withRel("downtimes"),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DocumentController.class)
                        .getAll(VersionType.LATEST.getValue(), result.getId(), 1, 10)).withRel("documents")
        );
    }
}
