package cz.syntea.app.iportal.domain.downtime.controller.link;

import cz.syntea.app.iportal.domain.downtime.controller.DowntimeController;
import cz.syntea.app.iportal.domain.downtime.model.Downtime;
import cz.syntea.app.iportal.domain.question.controller.QuestionController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * Třída přidávající linky k Downtime
 * @author kuchejda
 * @since 2022-03-30
 */
@Component
public class DowntimeModelAssembler implements RepresentationModelAssembler<Downtime, EntityModel<Downtime>> {

    @Override
    @NonNull
    public EntityModel<Downtime> toModel(@NonNull Downtime downtime) {
        return EntityModel.of(downtime,
                //WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(LocationController.class).getById(downtime.getLocation().getId())).withRel("location"), TODO dodělat endpoint
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DowntimeController.class).getById(downtime.getId())).withSelfRel(),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DowntimeController.class).getAll(downtime.getLocation().getId(), 1, 10)).withRel("downtimes"),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(QuestionController.class).getAll(downtime.getId())).withRel("questions")
        );
    }
}
