package cz.syntea.app.iportal.domain.location.model;

import cz.syntea.app.iportal.domain.location.def.LocationType;
import lombok.Value;

import java.util.Arrays;

/**
 * Třída reprezentující lokaci v systému (část IS SKP, domov, technologie...)
 * @author kuchejda
 * @since 2022-05-03
 */
@Value
public class Location {
    long id;
    String system;
    String subsystem;
    String userWebApplication;
    String adminWebApplication;
    String channel;
    LocationType specialType;

    /**
     * Vrátí cestu k dané lokaci v rámci IS SKP
     */
    String getPath() {
        if (specialType != null) {
            return ""; // Pro speciální typy lokací není cesta potřeba
        }
        String result = "";
        String[] parts = {system, subsystem, userWebApplication, adminWebApplication, channel};
        return Arrays.stream(parts)
                .filter(part -> part != null && !part.equals(""))
                .map(part -> "/" + part)
                .reduce("", (acc, part) -> acc + part)
                .substring(1); // úvodní '/' není potřeba
    }
}
