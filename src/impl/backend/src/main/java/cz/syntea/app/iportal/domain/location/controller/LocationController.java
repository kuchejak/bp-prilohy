package cz.syntea.app.iportal.domain.location.controller;

import cz.syntea.app.iportal.domain.location.controller.dto.EntityModelLocationTreeDto;
import cz.syntea.app.iportal.domain.location.dto.LocationTreeDto;
import cz.syntea.app.iportal.domain.location.service.LocationService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

/**
 * Controller pro lokace
 * @author kuchejda
 * @since 2022-03-26
 */
@RestController
@RequestMapping("/api/v1/locations")
@CrossOrigin
public class LocationController {

    private final LocationService locationService;

    private final RepresentationModelAssembler<LocationTreeDto, EntityModel<EntityModelLocationTreeDto>> modelAssembler;

    public LocationController(LocationService locationService, RepresentationModelAssembler<LocationTreeDto, EntityModel<EntityModelLocationTreeDto>> modelAssembler) {
        this.locationService = locationService;
        this.modelAssembler = modelAssembler;
    }

    @GetMapping(produces = "application/hal+json;charset=UTF-8")
    public ResponseEntity<CollectionModel<EntityModel<EntityModelLocationTreeDto>>> getAll() {
        return ResponseEntity.ok().body(
                CollectionModel.of(
                        locationService.findAll().stream().map(modelAssembler::toModel).collect(Collectors.toList())
                )
        );
    }
}
