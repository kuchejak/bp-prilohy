package cz.syntea.app.iportal.domain.location.dto;

import cz.syntea.app.iportal.domain.location.def.LocationType;
import lombok.Value;

import java.util.List;

/**
 * Třída pro stormovou reprezentaci lokací v systému. Parametrizovaný typ u položky children umožňuje použít jak LocationTreeDto,
 * tak EntityModel<LocationTreeDto>, tedy třídu rozšířenou o HATEOAS linky
 * @author kuchejda
 * @since 2022-04-27
 */
@Value
public class LocationTreeDto {
    long id;
    String name;
    LocationType specialType;
    List<LocationTreeDto> children;
}
