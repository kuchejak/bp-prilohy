package cz.syntea.app.iportal.domain.question;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author kuchejda
 * @since 2022-05-08
 */
@Configuration
@ComponentScan
public class QuestionConfig {
}
