package cz.syntea.app.iportal.domain.location.def;

/**
 * Speciální typy lokací v systému.
 * @author kuchejda
 * @since 2022-04-27
 */
public enum LocationType {
    Home,
    Technology,
    Info,
    SystemRoot,
    Emails
}
