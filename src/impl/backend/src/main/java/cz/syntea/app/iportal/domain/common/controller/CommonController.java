package cz.syntea.app.iportal.domain.common.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Společný controller pro operace, které se nehodí jinam
 * @author kuchejda
 * @since 2022-03-26
 */
@RestController
@RequestMapping("/api/v1/")
@CrossOrigin
public class CommonController {
}
