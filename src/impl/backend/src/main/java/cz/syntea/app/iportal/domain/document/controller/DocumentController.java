package cz.syntea.app.iportal.domain.document.controller;

import cz.syntea.app.iportal.domain.document.def.VersionType;
import cz.syntea.app.iportal.domain.document.dto.DocumentDto;
import cz.syntea.app.iportal.domain.document.model.Document;
import cz.syntea.app.iportal.domain.document.service.DocumentService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Collectors;

/**
 * Controller pro dokumenty
 * @author kuchejda
 * @since 2022-03-26
 */
@RestController
@RequestMapping("/api/v1/documents")
@CrossOrigin
public class DocumentController {

    private final DocumentService documentService;

    private final RepresentationModelAssembler<DocumentDto, EntityModel<DocumentDto>> documentDtoModelAssembler;
    private final RepresentationModelAssembler<Document, EntityModel<Document>> documentModelAssembler;

    public DocumentController(DocumentService documentService, RepresentationModelAssembler<DocumentDto, EntityModel<DocumentDto>> modelAssembler, RepresentationModelAssembler<Document, EntityModel<Document>> documentModelAssembler) {
        this.documentService = documentService;
        this.documentDtoModelAssembler = modelAssembler;
        this.documentModelAssembler = documentModelAssembler;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<EntityModel<DocumentDto>>> getAll(
            @RequestParam(defaultValue = "latest") String version,
            @RequestParam(defaultValue = "1") long idLocation,
            @RequestParam(defaultValue = "1") int page, // Stránkování začíná 1
            @RequestParam(defaultValue = "10") int size)
    {
        VersionType versionType = VersionType.getByValue(version)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown version type '" + version + "' specified"));
        return ResponseEntity.ok().body(
                CollectionModel.of(
                        documentService.findAll(versionType, idLocation, page, size).stream().map(documentDtoModelAssembler::toModel).collect(Collectors.toList())
                )
        );
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<EntityModel<Document>> getById(@PathVariable long id) {
        Document result = documentService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"Document with id = " + id + " does not exist"));

        return ResponseEntity
                .ok()
                .body(documentModelAssembler.toModel(result));
    }
}
