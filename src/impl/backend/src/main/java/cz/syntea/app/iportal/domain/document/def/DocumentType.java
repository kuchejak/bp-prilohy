package cz.syntea.app.iportal.domain.document.def;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Typy dokumentů, dělení převzaté z 83099
 * TODO Zhodnotit, jestli je dané dělení stále relevantní (až bude dostatek informací)
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum DocumentType {
    SR(DocumentExtension.PDF, "Specifikace rozhraní"), // TODO Jsou zde využity české názvy, chce to slovenské
    WSDL(DocumentExtension.XML, "Definice rozhraní WSDL"),
    PP(DocumentExtension.PDF, "Popis chyby"),
    AP(DocumentExtension.PDF, "Administrátorská příručka"),
    UM(DocumentExtension.PDF, "Uživateslký manuál"),
    TP(DocumentExtension.PDF, "Technický projekt"),
    UI(DocumentExtension.PDF, "Návrh GUI"),
    OD(DocumentExtension.PDF, "Obecný dokument"),
    TECH(DocumentExtension.PDF, "Technologický dokument");

    private final DocumentExtension extension;
    private final String description;

    DocumentType(DocumentExtension extension, String description) {
        this.extension = extension;
        this.description = description;
    }

    public DocumentExtension getExtension() {
        return extension;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Podporované koncovky souborů
     */
    public enum DocumentExtension {
        PDF(".pdf"),
        XML(".xml");

        private final String extension;

        DocumentExtension(String extension) {
            this.extension = extension;
        }

        public String getExtension() {
            return extension;
        }
    }
}
