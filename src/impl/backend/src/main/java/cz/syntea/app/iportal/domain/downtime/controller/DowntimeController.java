package cz.syntea.app.iportal.domain.downtime.controller;

import cz.syntea.app.iportal.domain.downtime.dto.DowntimeDto;
import cz.syntea.app.iportal.domain.downtime.model.Downtime;
import cz.syntea.app.iportal.domain.downtime.service.DowntimeService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Collectors;

/**
 * REST Controller pro odstávky.
 *
 * @author kuchejda
 * @since 2022-03-25
 */
@RestController
@RequestMapping("/api/v1/downtimes")
@CrossOrigin
public class DowntimeController {

    private final DowntimeService downtimeService;

    private final RepresentationModelAssembler<DowntimeDto, EntityModel<DowntimeDto>> downtimeDtoModelAssembler;

    private final RepresentationModelAssembler<Downtime, EntityModel<Downtime>> downtimeModelAssembler;

    public DowntimeController(DowntimeService downtimeService,
                              RepresentationModelAssembler<DowntimeDto, EntityModel<DowntimeDto>> downtimeDtoModelAssembler,
                              RepresentationModelAssembler<Downtime, EntityModel<Downtime>> downtimeModelAssembler) {
        this.downtimeService = downtimeService;
        this.downtimeDtoModelAssembler = downtimeDtoModelAssembler;
        this.downtimeModelAssembler = downtimeModelAssembler;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<EntityModel<DowntimeDto>>> getAll(
            @RequestParam(defaultValue = "1") long idLocation,
            @RequestParam(defaultValue = "1") int page, // Stránkování začíná 1
            @RequestParam(defaultValue = "10") int size) {
        return ResponseEntity.ok().body(
                CollectionModel.of(
                        downtimeService.findAll(idLocation, page, size).stream().map(downtimeDtoModelAssembler::toModel).collect(Collectors.toList())
                )
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<Downtime>> getById(@PathVariable long id) {
        Downtime result = downtimeService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Downtime with id = " + id + " does not exist"));

        return ResponseEntity
                .ok()
                .body(downtimeModelAssembler.toModel(result));
    }
}
