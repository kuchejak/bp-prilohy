package cz.syntea.app.iportal.domain.document.controller.link;

import cz.syntea.app.iportal.domain.document.controller.DocumentController;
import cz.syntea.app.iportal.domain.document.def.VersionType;
import cz.syntea.app.iportal.domain.document.dto.DocumentDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * Třída přidávající linky k DocumentDTO
 * @author kuchejda
 * @since 2022-04-06
 */
@Component
public class DocumentDtoModelAssembler implements RepresentationModelAssembler<DocumentDto, EntityModel<DocumentDto>> {

    @Override
    @NonNull
    public EntityModel<DocumentDto> toModel(@NonNull DocumentDto documentDto) {
        return EntityModel.of(documentDto,
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DocumentController.class).getById(documentDto.getId())).withSelfRel(),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DocumentController.class).getAll(
                        VersionType.LATEST.getValue(),
                        documentDto.getIdLocation(),
                        1,
                        10)).withRel("documents")
        );
    }

}
