package cz.syntea.app.iportal.domain.location.service;

import cz.syntea.app.iportal.domain.location.dto.LocationTreeDto;
import cz.syntea.app.iportal.domain.location.model.Location;

import java.util.List;
import java.util.Optional;

/**
 * Srvica pro práci s lokacemi
 * @author kuchejda
 * @since 2022-04-27
 */
public interface LocationService {

    Optional<Location> findById(long id);

    /**
     * Vrátí všechny existující lokace
     */
    List<LocationTreeDto> findAll();
}
