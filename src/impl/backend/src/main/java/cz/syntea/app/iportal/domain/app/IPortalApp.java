package cz.syntea.app.iportal.domain.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * @author kuchejda
 * @since 2022-03-24
 */
@Import(IPortalAppConfig.class)
@SpringBootApplication
public class IPortalApp {
    public static void main(String[] args) {
        SpringApplication.run(IPortalApp.class, args);
    }
}
