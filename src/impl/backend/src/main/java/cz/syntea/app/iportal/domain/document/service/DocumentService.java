package cz.syntea.app.iportal.domain.document.service;

import cz.syntea.app.iportal.domain.document.def.VersionType;
import cz.syntea.app.iportal.domain.document.dto.DocumentDto;
import cz.syntea.app.iportal.domain.document.model.Document;

import java.util.List;
import java.util.Optional;

/**
 * Rohraní pro práci s dokumenty
 * @author kuchejda
 * @since 2022-04-06
 */
public interface DocumentService {

    /**
     * Vrátí všechny dokumenty (stránkovaně)
     * @param versionType Typ verze dokumentů
     * @param idLocation Id lokace, pro kterou mají být vráceny dokumenty
     * @param page Stránka, která má být vrácena
     * @param size Velikost stránky
     */
    List<DocumentDto> findAll(VersionType versionType, long idLocation, int page, int size);

    /**
     * Vrátí konkrétní dokument podle zadaného id.
     * @param id Id dokumentu, který se má najít
     * @return Dokumenty odpovídající danému id, nebo Optional.empty, pokud takový dokument neexistuje
     */
    Optional<Document> findById(long id);
}
