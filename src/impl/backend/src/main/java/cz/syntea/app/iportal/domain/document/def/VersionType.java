package cz.syntea.app.iportal.domain.document.def;

import java.util.Optional;

/**
 * Výčet reprezenující typ verze dokumentu. Stringová hodnota se používa pro URL paramtry.
 */
public enum VersionType {
    ALL("all"), // Reprezentuje všechny verze
    LATEST("latest"); // Rerprezentuje nejnovější verzi

    private final String value;

    VersionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    /**
     * Vrátí instnaci podle zadané hodnoty, např. pro vstup "all" vrátí VersionType.ALL
     * @param value Hodnota, která se má převést na instanci
     * @return Instanci podle zadané hodnoty, nebo Optional.empty, pokud se daná hodnota neshoduje s žádným typem
     */
    public static Optional<VersionType> getByValue(String value) {
        for (VersionType type : values()) {
            if (type.getValue().equals(value)) {
                return Optional.of(type);
            }
        }
        return Optional.empty();
    }

    @Override
    public String toString() {
        return "VersionType{" +
                "value='" + value + '\'' +
                '}';
    }
}
