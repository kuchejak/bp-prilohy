package cz.syntea.app.iportal.domain.question.controller.link;

import cz.syntea.app.iportal.domain.question.model.QuestionWithAnswer;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * Třída přidávající linky k {@link QuestionWithAnswer}
 * @author kuchejda
 * @since 2022-05-08
 */
@Component
public class QuestionModelAssembler implements RepresentationModelAssembler<QuestionWithAnswer, EntityModel<QuestionWithAnswer>> {

    @Override
    @NonNull
    public EntityModel<QuestionWithAnswer> toModel(@NonNull QuestionWithAnswer questionWithAnswer) {
        return EntityModel.of(questionWithAnswer);
    }
}
