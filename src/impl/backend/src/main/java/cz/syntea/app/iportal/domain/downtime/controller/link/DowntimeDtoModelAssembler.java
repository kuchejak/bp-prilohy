package cz.syntea.app.iportal.domain.downtime.controller.link;

import cz.syntea.app.iportal.domain.downtime.controller.DowntimeController;
import cz.syntea.app.iportal.domain.downtime.dto.DowntimeDto;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * Třída přidávající linky k DowntimeDto
 * @author kuchejda
 * @since 2022-03-30
 */
@Component
public class DowntimeDtoModelAssembler implements RepresentationModelAssembler<DowntimeDto, EntityModel<DowntimeDto>> {

    @Override
    @NonNull
    public EntityModel<DowntimeDto> toModel(@NonNull DowntimeDto downtimeDto) {
        return EntityModel.of(downtimeDto,
                //WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(LocationController.class).getById(downtimeDto.getIdLocation())).withRel("location"), TODO dodělat endpoint
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DowntimeController.class).getById(downtimeDto.getId())).withSelfRel(),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(DowntimeController.class).getAll(downtimeDto.getIdLocation(), 1, 10)).withRel("downtimes")
        );
    }
}
