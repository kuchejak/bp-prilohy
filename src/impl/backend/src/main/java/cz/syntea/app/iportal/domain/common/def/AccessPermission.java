package cz.syntea.app.iportal.domain.common.def;

/**
 * Výčet reprezentující práva v systému
 * @author kuchejda
 * @since 2022-05-02
 */
public enum AccessPermission {
    ALL,
    SKP_ONLY
}
