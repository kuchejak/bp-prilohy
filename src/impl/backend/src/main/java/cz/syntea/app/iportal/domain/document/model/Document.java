package cz.syntea.app.iportal.domain.document.model;

import cz.syntea.app.iportal.domain.common.def.AccessPermission;
import cz.syntea.app.iportal.domain.document.def.DocumentType;
import cz.syntea.app.iportal.domain.location.model.Location;
import lombok.Value;

import java.time.LocalDateTime;

/**
 * Třída reprezentující metadata souboru (neobsahuje samotný obsah souboru)
 * @author kuchejda
 * @since 2022-05-02
 */
@Value
public class Document {
   long id;
   long documentNumber;
   String title;
   DocumentType documentType;
   String version;
   String reasonForInsertion;
   LocalDateTime dateOfChange;
   AccessPermission accessPermission;
   Location location;
}
