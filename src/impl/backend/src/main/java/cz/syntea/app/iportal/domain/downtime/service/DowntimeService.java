package cz.syntea.app.iportal.domain.downtime.service;

import cz.syntea.app.iportal.domain.downtime.dto.DowntimeDto;
import cz.syntea.app.iportal.domain.downtime.model.Downtime;

import java.util.List;
import java.util.Optional;

/**
 * Rozrhaní pro práci s odstávkami
 * @author kuchejda
 * @since 2022-03-27
 */
public interface DowntimeService {
    /**
     * Vrátí všechny odstávky (stránkovaně)
     * @param idLocation Id lokace, pro kterou mají být vráceny odstávky
     * @param page Stránka, která má být vrácena
     * @param size Velikost stránky
     */
    List<DowntimeDto> findAll(long idLocation, int page, int size);

    /**
     * Vrátí konkrétní odstávku podle zadaného id.
     * @param id Id odstávky, která se má najít
     * @return Odstávku odpovídající danému id, nebo Optional.empty, pokud taková odstávka neexistuje
     */
    Optional<Downtime> findById(long id);
}
