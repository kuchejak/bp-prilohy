package cz.syntea.app.iportal.domain.question.controller;

import cz.syntea.app.iportal.domain.question.model.QuestionWithAnswer;
import cz.syntea.app.iportal.domain.question.service.QuestionService;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

/**
 * REST Controller pro otzázky a odpovědi
 *
 * @author kuchejda
 * @since 2022-05-08
 */
@RestController
@RequestMapping("/api/v1/questions")
@CrossOrigin
public class QuestionController {

    private final QuestionService questionService;

    private final RepresentationModelAssembler<QuestionWithAnswer, EntityModel<QuestionWithAnswer>> questionModelAssembler;

    public QuestionController(QuestionService questionService, RepresentationModelAssembler<QuestionWithAnswer, EntityModel<QuestionWithAnswer>> questionModelAssembler) {
        this.questionService = questionService;
        this.questionModelAssembler = questionModelAssembler;
    }

    @GetMapping
    public ResponseEntity<CollectionModel<EntityModel<QuestionWithAnswer>>> getAll(
            @RequestParam(defaultValue = "1") long idEntity
    ) {
        return ResponseEntity.ok().body(
                CollectionModel.of(
                        questionService.findAll(idEntity).stream().map(questionModelAssembler::toModel).collect(Collectors.toList())
                )
        );
    }


}
