package cz.syntea.app.iportal.domain.document.service.impl;

import com.github.javafaker.Faker;
import cz.syntea.app.iportal.domain.common.def.AccessPermission;
import cz.syntea.app.iportal.domain.document.def.DocumentType;
import cz.syntea.app.iportal.domain.document.def.VersionType;
import cz.syntea.app.iportal.domain.document.dto.DocumentDto;
import cz.syntea.app.iportal.domain.document.model.Document;
import cz.syntea.app.iportal.domain.document.service.DocumentService;
import cz.syntea.app.iportal.domain.location.model.Location;
import cz.syntea.app.iportal.domain.location.service.LocationService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Defaultní servica pro práci s dokumenty
 * @author kuchejda
 * @since 2022-04-06
 */
@Service
public class DefaultDocumentService implements DocumentService {

    private final Faker faker = new Faker(new Locale("sk"));

    private final LocationService locationService;

    public DefaultDocumentService(LocationService locationService) {
        this.locationService = locationService;
    }

    @Override
    public List<DocumentDto> findAll(VersionType versionType, long idLocation, int page, int size) {
        return generateSampleDocumentsDto(size);
    }

    @Override
    public Optional<Document> findById(long id) {
        return Optional.of(generateSampleDocument(id));
    }

    private List<DocumentDto> generateSampleDocumentsDto(int count) {
        List<DocumentDto> result = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            result.add(this.generateSampleDocumentDto(i));
        }
        return result;
    }

    private DocumentDto generateSampleDocumentDto(long id) {
        // change
        LocalDateTime change = faker.date().between(faker.date().past(5, TimeUnit.DAYS), faker.date().future(5, TimeUnit.DAYS))
                .toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        return new DocumentDto(id,
                faker.number().numberBetween(10000, 99999),
                faker.number().numberBetween(1, 99999),
                getRandomDocumentType(),
                faker.company().buzzword() + "_" + "info",
                faker.app().version(),
                change,
                faker.lorem().word() + "/" + faker.lorem().word()
        );
    }

    private Document generateSampleDocument(long id) {
        DocumentDto documentDto = generateSampleDocumentDto(id);
        AccessPermission accessPermission = getRandomAccessPermission();
        String reasonForInsertion = faker.shakespeare().hamletQuote();
        Location location = locationService.findById(faker.number().numberBetween(1, 10000)).get();
        return new Document(id,
                documentDto.getDocumentNumber(),
                documentDto.getTitle(),
                documentDto.getDocumentType(),
                documentDto.getVersion(),
                reasonForInsertion,
                documentDto.getDateOfChange(),
                accessPermission,
                location
        );
    }

    private DocumentType getRandomDocumentType() {
        DocumentType[] types = {DocumentType.AP, DocumentType.OD, DocumentType.PP, DocumentType.SR, DocumentType.TP};
        Random random = new Random();
        int idx = random.nextInt(types.length);
        return types[idx];
    }

    private AccessPermission getRandomAccessPermission() {
        AccessPermission[] permissions = {AccessPermission.ALL, AccessPermission.SKP_ONLY};
        Random random = new Random();
        int idx = random.nextInt(permissions.length);
        return permissions[idx];
    }

}
