# Informační portál Slovenské kanceláře pojistitelů

Navrhněte a v jazyce java částečně implementujte informační portál Slovenské kanceláře pojistitelů. Pro správu dokumentů využijte dokumentační systém SynDOC, jako databázi zvolte PostgreSQL a pro frontend použijte vhodný framework. 
V teoretické části pojednejte o dokumentačním systému SynDOC. Vysvětlete, co to je „pohledová struktura“ a jak jsou do ní dokumenty řazeny podle svých metadat. Dále stručně představte zvolený frontend framework.
Proveďte funkční a datovou analýzu systému a navrhněte vhodné uživatelské rozhraní a plán testů.
V praktické části implementujte aspoň některé části navrhovaného řešení.