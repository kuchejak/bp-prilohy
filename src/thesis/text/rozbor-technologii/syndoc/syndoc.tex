\section{SynDOC}
\label{chpt:SynDOC}
SynDOC je dokumentační systém vyvíjený ve společnosti Syntea, který se zaměřuje na ukládání, správu, organizaci a verzování\footnote{To znamená, že pro každý dokument může být uloženo více různých verzí.} dokumentů v podstatě libovolného formátu. Jednou z nejzajímavějších funkcionalit tohoto dokumentačního systému je možnost vytvářet takzvané pohledové struktury, jejichž princip bude vysvětlen dále (sekce \ref{sec:PohlStr}) a které se pro využití v informačním portálu velice hodí, protože implementaci v mnoha ohledech usnadní.

Prvotní verze je v provozu již několik let a byla mimo jiné využita i při tvorbě současného informačního portálu. Aktuálně je už nicméně ve vývoji verze 2.0, která původní verzi v mnoha ohledech vylepšuje, hlavní principy fungování však zůstávají stejné.

S dokumentačním systémem SynDOC se pracuje podobně jako s jakoukoli běžnou knihovnou, tedy skrze zprostředkované aplikační rozhraní (sada veřejných metod), skrze které lze se systémem interagovat.

Cílem této části je popsat hlavní principy tohoto dokumentačního systému, hlavně se zaměřením na pohledové struktury, které budou při implementaci portálu využity.

\subsection{Metadata a skupiny dokumentů}
\label{sec:meta}
K jednotlivým dokumentům mohou uživatelé ukládat metadata, tedy přídavné informace, které daný dokument charakterizují (metadata přidaná uživateli jsou společná pro všechny verze daného dokumentu). Metadata se popisují pomocí takzvaných atributů, které se skládají z názvu a hodnoty\footnote{Obsahují navíc i několik dalších příznaků, které ale pro pochopení popisované problematiky nejsou důležité.}. Hodnoty mohou mít různé datové typy, konkrétně \verb|STRING| (textový řetězec), \verb|NUMBER| (celé číslo), \verb|FLOAT| (desetinné číslo), \verb|DATETIME| (datum a čas, může být omezeno i na datum bez času) a \verb|BOOLEAN| (hodnoty \verb|true| a \verb|false|, tedy pravda a nepravda). Speciálním datovým typem je také výčet, který může nabývat pouze předem stanovených hodnot. Jednotlivé datové typy lze v případě potřeby ještě více konkretizovat -- například u textového řetězce může být omezena délka, u čísel a dat je možné omezit rozsah. \cite[4]{Syntea:SD:ER} 

Například tak můžeme mít atribut \textit{Název} s hodnotou typu \verb|STRING|, nebo atribut \textit{Čas zveřejnění} s hodnotou typu \verb|DATETIME|.

Dokument je možné přidat do takzvané skupiny, která určuje, jaké atributy v ní zařazené dokumenty mohou či musí mít. Skupina navíc může atributy podědit z jedné či více\footnote{Základním pravidlem v takovém případě je, že se nikde ve stromu dědění nesmí vyskytnout stejně pojmenovaný atribut více než jednou, čímž lze předejít problémům s konflikty. Ve stromu dědění také nesmí vzniknout zacyklení.} jiných skupin, což umožňuje postupné konkretizování typů dokumentů. SynDOC pracuje i s verzemi skupin, aby bylo možné atributy skupiny v případě potřeby upravit bez nutnosti zasahovat do všech existujících dokumentů (těch může být velké množství) -- detaily fungování verzí skupin však nejsou pro pochopení zde popisovaných principů důležité. \cite[4--6]{Syntea:SD:ER}

Atributy ve skupině lze omezit i nad rámec konkrétních datových typů. SynDOC nabízí možnost nastavit povinnost atributu (tedy jestli atribut musí být vyplněn, nebo jestli může být ponechán prázdný) a možnost vyžadovat pro atribut (nebo skupinu atributů) unikátní hodnotu (respektive kombinaci hodnot), což se hodí například pro identifikátory. Kromě těchto základních nástrojů je k dispozici ještě takzvaný \enquote{checker skupiny}, který dovoluje hodnotu atributu (či více atributů najednou) pro daný dokument verifikovat pomocí jazyka SynDOC2\footnote{Speciální jazyk, který dokumentační systém SynDOC přímo podporuje -- umožňuje jednoduchou práci se skupinami a atributy.}, PLSQL funkce či pomocí Java bean -- hodnoty atributů lze tak omezit prakticky libovolně, jedinou limitací je, že \enquote{checker skupiny} pracuje vždy pouze s daným dokumentem (například kontrola unikátnosti musí naopak pracovat se všemi dokumenty dané skupiny najednou). \cite[4--5]{Syntea:SD:ER}

Kdybychom například chtěli evidovat faktury, mohli bychom si vytvořit skupinu \textit{Faktura} s atributy v tabulce \ref{tab:SD:Skupina} (jedná se pouze o jednoduchý příklad, reálné použití by pravděpodobně bylo obsáhlejší). Podle těchto atributů bychom pak mohli faktury podle potřeby organizovat a také by mohly posloužit pro snadnou orientaci předtím, než uživatel dokument otevře a podívá se na jeho obsah.

\begin{table}[h]
    \centering
    \caption{Příklad skupiny}
    \label{tab:SD:Skupina}
    \begin{tabular}{|l|l|l|}
        \hline
        \textbf{Název atributu} & \textbf{Datový typ} & \textbf{Omezení}                      \\ \hline
        Stručný popis obsahu    & \verb|STRING|       & Povinné                               \\
        Celková částka          & \verb|NUMBER|       & Povinné                               \\
        Datum vystavení         & \verb|DATETIME|     & Povinné                               \\
        Datum splatnosti        & \verb|DATETIME|     & Musí být po datu vystavení, povinné   \\
        Datum zaplacení         & \verb|DATETIME|     & Musí být po datu vystavení, nepovinné \\
        \hline
    \end{tabular}
\end{table}

Kromě uživatelem definovaných atributů má každý dokument i takzvané systémové atributy, které udávají například název, datum a čas uložení nebo verzi dokumentu. \cite[12]{Syntea:SD:SpecF} 

\subsection{Adresářové struktury}
\label{sec:AdrStr}
Adresářová struktura je jednou ze dvou struktur pro organizaci dokumentů, které SynDOC nabízí. V této struktuře jsou dokumenty ručně řazeny uživatelem do adresářů, podobně jako v jakémkoli běžném správci souborů (například v průzkumníku souborů v systému Windows). \cite[6]{Syntea:SD:ER}

Výhodou tohoto přístupu je jednoduchost a také fakt, že uživatelé mají tento přístup zažitý a jeho pochopení by tak pro ně mělo být jednoduché a intuitivní. SynDOC umožňuje mít adresářových struktur více, takže každý uživatel si může své dokumenty organizovat přesně podle své potřeby.

Hlavní nevýhodou je fakt, že adresářová struktura je spravována ručně a dokumenty tak nemusí být nutně zařazeny ve správných adresářích, každý dokument navíc může být v adresářové struktuře umístěn nejvýše jednou, což může být v některých případech značně omezující. Jak moc velký problém tato negativa představují pak záleží na konkrétním použití a potřebách uživatelů.

\subsection{Pohledové struktury}
\label{sec:PohlStr}
Pohledové struktury jsou jednou z nejsilnějších součástí dokumentačního systému SynDOC. Umožňují automatickou organizaci dokumentů\footnote{Pohledová struktura ve skutečnosti pracuje s jednotlivými \textit{verzemi} dokumentů, pro srozumitelnější vysvětlení základního principu však bude lepší tento detail zanedbat.} do jednotlivých uzlů (alternativa adresáře v pohledové struktuře) na základně jejich metadat. Pokud chce uživatel dokument v pohledové struktuře přesunout, musí mu změnit metadata tak, aby odpovídala cílovému umístnění (toto nemusí být v některých případech možné, například pokud daný uzel pracuje s neměnnými systémovými atributy). \cite[5]{Syntea:SD:ER}

Velkou výhodou tohoto přístupu je, že po počáteční definici pohledové struktury a skupin souborů už uživatel pouze při vytváření či úpravě dokumentu nastaví správná metadata (některá může případně program i automaticky odvodit) a daný dokument je pak podle nich správně zařazen přesně tam, kam patří. V praxi mohou do dokumentačního systému proudit tisíce dokumentů a jejich automatická a správná organizace v pohledových strukturách je nedocenitelná. Jednotlivé dokumenty také mohou být v pohledové struktuře vícekrát, což může být přínosné (například můžeme mít uzel, ve kterém se zobrazují všechny dokumenty změněné v nedávné době a pak další uzly, kde jsou dokumenty řazeny podle kategorií, do kterých patří).

Mírnou nevýhodou oproti adresářové struktuře (\ref{sec:AdrStr}) je fakt, že uživatelé se musí s pohledovou strukturou naučit pracovat, protože se chová trochu odlišně od toho, na co jsou většinou zvyklí. Při dobře navrženém uživatelském rozhraní však může být práce s pohledovou strukturou jednoduchá a velice podobná práci se strukturou adresářovou. Nevýhodou je fakt, že je potřeba, ideálně dopředu, správně rozvrhnout skupiny dokumentů a pohledové struktury podle nich nadefinovat (\ref{sec:PohlStrVyt}) -- je tak potřeba dopředu zvážit, jestli se tato počáteční časová investice vyplatí.

\subsubsection{Vytvoření pohledové struktury}
\label{sec:PohlStrVyt}
Pohledová struktura vzniká na základě takzvaných definičních uzlů. Tyto definiční uzly mají tři nejdůležitější položky: 

\begin{itemize}
    \item Odkaz na rodiče -- využívá se pro vytvoření grafu typu zakořeněný strom, do kterého jsou definiční uzly řazeny. Odkaz na rodiče musí mít uvedeny všechny definiční uzly s výjimkou uzlu kořenového.
    \item Podmínka uzlu -- jedná se o výraz v jazyce SynDOC2, který pro dokument na vstupu určí, jestli podmínku splňuje a má být zařazen do odpovídajícího uzlu, nebo ne.
    \item Výraz pro vytvoření názvu uzlu -- jedná se o opět o výraz v jazyce SynDOC2, který může být dvou základních typů:
          \begin{itemize}
              \item Fixní -- název uzlu je napevno daný (zadaný výraz pro vytvoření názvu uzlu vrací vždy stejný název, bez ohledu na vstup), z definičního uzlu vznikne nejvýše jeden uzel, který bude tento název mít.
              \item Dynamický -- výraz vrací název uzlu podle dokumentu na vstupu (respektive podle jeho metadat), díky čemuž může z jednoho definičního uzlu vzniknout více různě pojmenovaných uzlů. Toto je velice užitečná a důležitá funkce, pokud nejsou dopředu známa všechna možná vstupní data. \cite[5--4]{Syntea:SD:ER}
          \end{itemize}
\end{itemize}

Definiční uzly tak jasně popisují, kam mají být konkrétní dokumenty ve struktuře umístěny. Na základě těchto definičních uzlů pak vznikají samotné uzly pohledové struktury (ty už mají konkrétní název), do nichž jsou zařazeny příslušné dokumenty (viz \ref{sec:PohlStrZar}). Uzly pohledové struktury, stejně jako definiční uzly, tvoří graf typu zakořeněný strom. \cite[7]{Syntea:SD:ER}

\subsubsection{Zařazení dokumentu do pohledové struktury}
\label{sec:PohlStrZar}
Po vložení nového dokumentu se dokumentační systém pokusí o jeho zařazení do všech existujících pohledových struktur. Při vkládání do pohledové struktury je daný dokument postupně testován definičními uzly, počínaje definičním uzlem v kořenu grafu\footnote{Jak už bylo zmíněno v \ref{sec:PohlStrVyt}, definiční uzly tvoří graf typu zakořeněný strom.}. V případě, že dokument splní podmínku právě kontrolovaného definičního uzlu, je zařazen do odpovídajícího uzlu a pokračuje se v testování pod-uzlů\footnote{Zde myšleno ve smyslu uzlů daného grafu, tyto pod-uzly jsou také definičními uzly.} a také definičních uzlů sousedních (dokument může být ve struktuře zařazen na více místech). Ve výsledné struktuře se dokument zobrazí u uzlů, do kterých je zařazen, pouze pokud není zařazen do žádného z jejich pod-uzlů. \cite[7]{Syntea:SD:ER} 

\subsubsection{Příklad využití}
Představme si program vyvíjený pro potřeby investigativních žurnalistů -- ti v rámci své práce vytvářejí a shromažďují různé typy dokumentů, například články, fotografie, audio nahrávky či videa, ke kterým se pak při své práci musí opakovaně vracet. 

Například, když žurnalisté zjišťují informace o nějaké osobě, potřebují si zobrazit všechny záznamy, které se této osoby týkají, navíc roztříděné podle nějakého kritéria (například podle času nebo podle místa).

Pro zjednodušení předpokládejme, že ke každému dokumentu jsou přiřazena tato metadata:
\begin{itemize}
    \item osoba, které se záznam týká
    \item místo, kterého se informace týká. Skládá se z města, ulice a čísla popisného
    \item čas, ke kterému se informace váže. Pro zjednodušení se skládá pouze z roku a měsíce
\end{itemize}
V praxi by metadata mohla být mnohem komplexnější, pro jednoduchý příklad ale tyto tři jednoduché položky postačí. 

Pro potřeby žurnalistů pak můžeme ná základě těchto metadat definovat různé pohledové struktury, podle kterých SynDOC dokumenty roztřídí. 

Například může být potřeba pohledová struktura, ve které budou zobrazeny informace nashromážděné pro jednotlivé osoby, členěné podle let a měsíců, kdy byly pořízeny, bez ohledu na místo. Pohledová struktura by pak mohla být definována takto:

~\\
\begin{minipage}{0.40\textwidth}
\begin{itemize}
    \item[$\color{enumgray}\bullet$] @Prijmeni @Jmeno
    \begin{itemize}
        \item[$\color{enumgray}\bullet$] @Rok
        \begin{itemize}
            \item[$\color{enumgray}\bullet$] @Mesic
        \end{itemize}
    \end{itemize}
\end{itemize}
\end{minipage}
\begin{minipage}{0.1\textwidth}
    $\color{enumgray}\longrightarrow$
\end{minipage}
\begin{minipage}{0.40\textwidth}
\begin{itemize}
    \item[$\color{enumgray}\blacktriangledown$] Falešný Marek
        \begin{itemize}
            \item[$\color{enumgray}\blacktriangledown$] 2010
                \begin{itemize}
                    \item[$\color{enumgray}\blacktriangledown$] Leden
                        \begin{itemize}
                            \item[$\color{enumgray}\bullet$] \textit{Rezidence.jpg}
                            \item[$\color{enumgray}\bullet$] \textit{Schůzka.mp3}
                        \end{itemize}
                    \item[$\color{enumgray}\blacktriangledown$] Srpen
                        \begin{itemize}
                            \item[$\color{enumgray}\bullet$] \textit{Zakazka.pptx}
                        \end{itemize}
                \end{itemize}
            \item[$\color{enumgray}\blacktriangleright$] 2012\footnotemark
            \item[$\color{enumgray}\blacktriangleright$] 2015
        \end{itemize}
    \item[$\color{enumgray}\blacktriangleright$] Podezřelý Jan 
\end{itemize}
\end{minipage}
\footnotetext{Položky označené pomocí $\color{enumgray}\blacktriangleright$ by se dále větvily podobně, jako položky označené pomocí $\color{enumgray}\blacktriangledown$, jejich obsah však není popsán pro zjednodušení příkladu.}
\\

Vlevo je naznačena definice pohledové struktury (jednotlivé body reprezentují název uzlu který vznikne na základě metadat vstupního souboru, použitá syntaxe je inspirovaná jazykem SynDOC2), vpravo je poté struktura, která vznikne po vložení několika souborů.

\pagebreak

Také by mohla být definována struktura, která záznamy rozdělí podle místa, bez ohledu na aktéry, kterých se týká:

~\\

\noindent
\begin{minipage}{0.40\textwidth}
\begin{itemize}
    \item[$\color{enumgray}\bullet$] @Mesto
    \begin{itemize}
        \item[$\color{enumgray}\bullet$] @Ulice
        \begin{itemize}
            \item[$\color{enumgray}\bullet$] @CisloPopisne
        \end{itemize}
    \end{itemize}
\end{itemize}
\end{minipage}
\begin{minipage}{0.1\textwidth}
    $\color{enumgray}\longrightarrow$
\end{minipage}
\begin{minipage}{0.40\linewidth}
\begin{itemize}
    \item[$\color{enumgray}\blacktriangledown$] Brno
        \begin{itemize}
            \item[$\color{enumgray}\blacktriangledown$] Evropská
                \begin{itemize}
                    \item[$\color{enumgray}\blacktriangledown$] 123/45
                        \begin{itemize}
                            \item[$\color{enumgray}\bullet$] \textit{Rezidence.jpg}
                        \end{itemize}
                    \item[$\color{enumgray}\blacktriangledown$] 45/321
                        \begin{itemize}
                            \item[$\color{enumgray}\bullet$] \textit{Schůzka.mp3}
                        \end{itemize}
                \end{itemize}
            \item[$\color{enumgray}\blacktriangleright$] U Jezera
            \item[$\color{enumgray}\blacktriangleright$] Příčná
        \end{itemize}
    \item[$\color{enumgray}\blacktriangleright$] Ostrava
\end{itemize}
\end{minipage}
\\
\\

Na příkladech je vidět, že ačkoliv dané pohledové struktury zobrazují stejné informace, jejich struktura se zásadně odlišuje. Díky tomu je možné informace organizovat přesně tak, jak to uživatelé potřebují.

Velkou výhodou tohoto přístupu je, že pohledová struktura se dokáže přizpůsobovat podle vložených dokumentů -- uživatelé pouze při vložení dokumentů zadají správná metadata a SynDOC se pak na základě definice pohledové struktury postará o vytvoření potřebných uzlů a zařazení dokumentů přesně tam, kam patří.