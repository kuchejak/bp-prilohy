Implementace ve složce src/impl je rozdělena na backend a frontend.

1. backend (Java 8)
Pro spuštění je nutné mít na počítači zprovozněnou javu a maven. Kompilaci a spuštění z příkazové řádky lze provést ve složce /backend pomocí příkazu "mvn clean package spring-boot:repackage && cd target && java -jar syntea-app-iportal-backend-1.0.jar"

2. frontend (React)
Pro spuštění je nutné mít nainstalované Node.js (lze ověřit v terminálu pomocí "npm -v"). Spuštění je poté možné z příkazové řádky ze složky /frontend pomocí "npm i && npm start".

V případě, že je spuštěn backend, frontendová část by měla zobrazit data. Pokud backend spuštěný není, frontend zobrazí pouze základní rozhraní bez dat.

---

Pozn. komentáře jsou v češtině, protože jsou tak běžně psány ve společnosti Syntea. 